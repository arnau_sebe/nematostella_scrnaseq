###############CONTENTS DESCRIPTION
####1. Scripts/   Code and dependencies to reproduce the figures and analysis (as described below). 

####2. UMI_tables/   Contains the single cell gene molecule counts per batch. 

####3. Annotation_and_conf_files/   Contains Nematostella gene annotations and conf files for the pipeline. Specifically:
#3.1 General_cell_module_annotation: contains the cell module ordering, meta-cluster classification, and color-coding used in the paper.
#3.2 Neuronal_cell_modules_annotation: same as above, for the neuronal re-clustering analysis (Figure 5).
#3.3 Nematostella_gene_ages:  Gene age defition, based on OrthoMCL clustering and parsimony (as described in the paper)
#3.4 Nematostella_gene_annotation: Different custom functional annotations (including Pfam (pfam.xfam.org) domain architectures and Blast Best Hits against different species)
#3.5 Nematostella_TF_annotation: Annotation of Nematostella transcription factors, including name, TF class and color-code used in the paper.
#3.6 Neurons_cells_ids_all: list of neuronal cell ids for re-clustering (Figure 5)
#3.7 black_list_genes: List of blacklisted genes


####4. Motif_analysis_input_tables/    Contains pre-computed tables for motif analysis. Inferred motifs were downloaded from CisBP (cisbp.ccbr.utoronto.ca). Specifically:
#4.1 CisBP_global_motif_max_val_quant_150bp.txt & CisBP_global_motif_max_val_quant_250bp.txt: precomputed motif PWM quantiles for 150bp and 250bp windows genome-wide (used for enhancer and promoter analysis, respectively).
#4.2 CisBP_max_enhancers & CisBP_max_promoters200_50.txt: Maximum motif energy observed in each enhancer and promoter in Nematostella genome (for each of the CisBP Nematostella motifs).
#4.3 Nvec_gene_lists_per_cell_module: Genes associated to each cell module.
#4.4 Nvec_CisBP.dictionary: Annotation of the inferred CisBP motifs.
#4.5 [[OPTIONAL]] Precomputed_Cell_modules_Enhancers_motifs_FDR.txt & Precomputed_Cell_modules_Promoters_motifs_FDR.txt: precomputed motif enrichment FDR for promoters/enhancers of the genes associated to each cell module.

####5. Precomputed_boostraps/    Contains pre-computed 1000 bootstrap replicates. Specifically:
#5.1 Global_analysis_bootstrap1000.Rdata
#5.2 Neuronal_analysis_bootstrap1000.RData


#####################################################################################################################################################
############################################  STEP-by-STEP ANALYSIS  ################################################################################
#####################################################################################################################################################

#Open a new R session in the main folder and run the commands described below.
#Install (if necessary) required R packages:
install.packages("plyr")
install.packages("dplyr")
install.packages("reshape2")
install.packages("zoo")
install.packages("scales")
install.packages("dendextend")
install.packages("RColorBrewer")


####Source scripts
require("devtools"); load_all("./Scripts/scrdb/")
require("methods")
require("Matrix")
require("ape")
require("matrixStats")
source("Scripts/Nvec_downstream_analysis.R")
source("./Scripts/Bootstraping.R")


###############COMPUTE CELL MODULES###########################
mars_base = "./UMI_tables"
bl=scan("Annotation_and_config_files/black_list_genes",what="")

sc_cl = sc_pipe(index_fn = "Annotation_and_config_files/MARS_Batches", 
		base_dir = mars_base, 
		batch_meta_attr = "MARS_BATCH", 
		mark_blacklist_terms=bl,
		mark.min_var_mean = NA,
		mark.sz_cor_norm_max= -0.05,
		mark.niche_T = 0.05,
		mark.min_tot=100,
		min_umi_n = 100,
		amb_epsilon=0.03,
		clust_knn=250,
		min_clust_size = 50,
		sample_n_batches = NA,
		filt_amb_on_clusts=T,
		filt_outliers_on_clusts=F)

filt_cl=scc_filter_bad_clusters(sc_cl,min_max_fold_change=9,k_knn_for_confu=30,min_self_confusion=0.2)


###Compute 1000 bootstrap. Takes >5h
boot1000=tg_piecewise_knn_graph_cover_bootstrap(t(as.matrix(filt_cl@feat_mat)),k_knn=187,min_clust_size=38,boot_ratio=0.75,N_boot=1000)
#Alternatively, load precomputed bootstrap:
load("Precomputed_boostraps/Global_analysis_bootstrap1000.Rdata")
#boot1000 is a list of 2 matrices: boot1000$coclust:   number of times cells have co-ocurred in the same cell module in 1000 bootstrap replicates.
								  #boot1000$num_trials: number of times cells have been co-sampled.


#Generate and load 2d clustering object (it also produces several summary plots)
sc_pipe_plots(filt_cl,
		mark_blacklist_terms=bl,
		focus_tfs_fn=NA,
		force_max_deg_2dproj = 8,
		store_rda_fn="nvec_cl_2d.Rda",
		T_edge_asym = F,
		K_2dproj = 80,
		K_cells_2dproj = 30,
		T_edge_2dproj = 0.02,
		restrict_edges_by_fpcor=T)
load("nvec_cl_2d.Rda")



#Load the cell module classification and color-coding scheme. Generate once aggregate module counts. 
scr_load_sn_table("Annotation_and_config_files/General_cell_module_annotation",filt_cl)
scr_gen_niche_counts(filt_cl,4)


########Figure 1##########
##Scatterplots and other descriptive plots in Figure 1
scr_Figure_1_statistics("Figure_1",sz_cor_threshold=-0.1)

##Color-coded 2d projection
sc_2d@cl_colors=as.character(scr_sn_table[order(scr_sn_table$niches),"color"])
scp_plot_clust_2d(sc_2d,fname="Figure_1_graph2d.png",cell_cols=cells_cols,plot_edges=T)

#Plot single cell gene expression footprints
scr_Figure_1_cmod_markers_ftp(filt_cl,bl,"Figure_1_sc_footprint.png",per_clust_genes=50,gene_min_fold=2.5,transverality_N=35)

#Boxplot metacluster bootstrap scores
scr_metacluster_boostrap_score(boot1000,filt_cl,1000)

########Figure 2##########
#Gene age enrichment plots
scr_phylosratigraphy_metaclusters(filt_cl,fc_threshold=2,PS_table_file="Annotation_and_config_files/Nematostella_gene_ages") 


########Figure 3##########
#Plot TF stats
scr_tfs_stats(filt_cl,"Figure_3_TF_families")
scr_tfs_per_niche("Figure_3_TFs_in_cell_modules")

#TF modules and footprint
scr_tf_tf_cor_footprint(filt_cl,min_FC=1.7,T_totumi=20,fuse_modules2=T)


########Figure 4##########
####Source specific code for motif analysis
source("./Scripts/Motif_analysis.r")

###1. PROMOTER analysis
#Load pre-computed motif energy tables for promoters, and gene lists and motif-TF name dictionary.
load_global_and_local_motif_max_quant(global_motif_quant_table_file="./Motif_analysis_input_tables/CisBP_global_motif_max_val_quant_250bp.txt",
                                     feature_motif_quant_table_file="./Motif_analysis_input_tables/CisBP_max_promoters200_50.txt",
                                     gene_lists_table_file="./Motif_analysis_input_tables/Nvec_gene_lists_per_cell_module",               
                                     dict_file="./Motif_analysis_input_tables/Nvec_CisBP.dictionary")

##Compute motif-motif correlations based on promoter energies. Will be used to collapse redundant motifs (resulting from the motif inference process in CisBP, as explained in the paper).
#Do it once and save the table. It takes a while. Will be input for downstream functions.
motif_cor_reduce_motifs(output_table_name="Motif_motif_cor_table.txt")

##We can plot the previously computed motif-motif correlations (but see specific usage of this function for Extended Data Fig 8a below)
plot_motif_motif_correlation(correlation_matrix_file="Motif_motif_cor_table.txt")

#OPTIONAL. Compute FDR motif enrichment table for ALL motifs. We define the quantile threshold of presence, based on the genome-wide distribution of energies (tables uplodaded in the first step).
#It takes quite some time, so we do it only once and save the resulting table. 
#--> If you want to skip this step, there is a precomputed Promoter_FDR table inside the Motif_analysis_input_tables folder: "Precompute_Cell_modules_Promoters_motifs_FDR.txt"
motif_enrichment_permutation(Quant_thresh=0.98,output_table_bn="Promoter_motifs")

##Plot heatmap enriched motifs in promoters of each cell module.
motif_enrichment_clustering(feature_type="promoters",output_fn="Figure_4a_Promoter_motif_enrichment_heatmap.png",motif_merge_cor_thrs=0.8,Quant_thresh=0.98,
                            motif_FDR_table_file="./Motif_analysis_input_tables/Precomputed_Cell_modules_Promoters_motifs_FDR.txt",write_tables=FALSE)

#Plot the motif-motif correlation matrix for Extended Data Fig 8a
plot_motif_motif_correlation(correlation_matrix_file="Motif_motif_cor_table.txt",
                              FDR_table="./Motif_analysis_input_tables/Precomputed_Cell_clusters_Promoter_motifs_FDR.txt")


###2. ENHANCER analysis
#Load pre-computed motif energy tables for enhancers, and gene lists and motif-TF name dictionary.
load_global_and_local_motif_max_quant(global_motif_quant_table_file="./Motif_analysis_input_tables/CisBP_global_motif_max_val_quant_150bp.txt",
                                     feature_motif_quant_table_file="./Motif_analysis_input_tables/CisBP_max_enhancers.txt",
                                     gene_lists_table_file="./Motif_analysis_input_tables/Nvec_gene_lists_per_cell_module",               
                                     dict_file="./Motif_analysis_input_tables/Nvec_CisBP.dictionary")
#OPTIONAL. Compute FDR motif enrichment table for ALL motifs. We define the quantile threshold of presence, based on the genome-wide distribution of energies (tables uplodaded in the first step).
#It takes quite some time, so we do it only once and save the resulting table. 
#--> If you want to skip this step, there is a precomputed Enhancer_FDR table inside the Motif_analysis_input_tables folder: "Precompute_Cell_modules_Enhancer_motifs_FDR.txt"
motif_enrichment_permutation(Quant_thresh=0.98,output_table_bn="Enhancer_motifs")

##Plot heatmap enriched motifs in enhancers of each cell module.
motif_enrichment_clustering(feature_type="enhancers",output_fn="Figure_4d_Enhancer_motif_enrichment_heatmap.png",motif_merge_cor_thrs=0.8,Quant_thresh=0.98,
                            motif_FDR_table_file="./Motif_analysis_input_tables/Precompute_Cell_modules_Enhancer_motifs_FDR.txt",write_tables=FALSE)



########Figure 5. Neuronal zooming analysis##########
##load neuronal cell ids
neurons_all=scan("Annotation_and_config_files/Neurons_cells_ids_all",what="")

##compute cell modules
sc_cl = sc_pipe(index_fn = "Annotation_and_config_files/MARS_Batches_for_Neurons",   
		base_dir = mars_base, 
		batch_meta_attr = "MARS_BATCH", 
		mark_blacklist_terms=bl,
		store_rda_fn="nvec_cl.Rda",
		mark.min_var_mean = NA,
		mark.sz_cor_norm_max= -0.05,
		mark.niche_T = 0.05,
		mark.min_tot=100,
		min_umi_n = 100,
		amb_epsilon=0.03, 
		clust_knn=150,
		min_clust_size = 30,
		sample_n_batches = NA,
		filt_amb_on_clusts=T,
		filt_outliers_on_clusts=F,
		cell_subset=neurons_all)
		
##run bootstraping 
boot1000=tg_piecewise_knn_graph_cover_bootstrap(t(as.matrix(sc_cl@feat_mat)),k_knn=115,min_clust_size=22,boot_ratio=0.75,N_boot=1000)
##or load precomputed bootstraps: 
load("Precomputed_boostraps/Neuronal_analysis_bootstrap1000.RData")

##fuse similar modules using bootstrap matrix
fused_cl=scr_merge_clusters_by_bootstrap(sc_cl,boot1000,0.2)

##compute cell module bootstrap support and module score (described in Methods section) and filter bad modules
module_bootstrap=scr_cluster_boostrap_score(boot1000,fused_cl,1000)
module_stats=scr_module_score(fused_cl)
module_score=module_stats[3,]
good_modules=names(which(module_score>=0.5 | module_bootstrap >= 0.7))
good_modules=setdiff(good_modules,"23") ## we kill the cell clusters composed by dead cells
filt_cl=scr_select_good_modules(fused_cl,good_modules)

##Load cell module anno 
scr_load_sn_table("Annotation_and_config_files/Neuronal_cell_modules_annotation",filt_cl)
scr_gen_niche_counts(filt_cl)

##Plot single cell gene footprint
scr_Figure_1_cmod_markers_ftp(filt_cl,bl,"Figure_5_sc_footprint.png",per_clust_genes=50,gene_min_fold=2,transverality_N=8)

#Neuronal TF modules and footprint
scr_tf_tf_cor_footprint(filt_cl,output_file="TF_map_nonames.png",fuse_modules2=T,nmodules=60,T_totumi=30,min_FC=1.6,module_fuse_thrs=0.6,plot_gene_names=T,h=6000,w=4000)

