# this is important for running this package from scripts
#' @import methods
NULL

# #' @example R/workflow_example.r

#' Single cell RNA-seq matrix
#'
#' a single cell RNA matrix interface. Does not do much beyond adding
#' specifically gene names, cell names and labels describing the type of data.
#' Methods for basic stat, gene selection, clustering are implemented separately
#'
#' @slot mat A sparse matrix containing the expression data
#' @slot Genes list of all genes
#' @slot Cells list of all cells
#' @slot stat_type Type of statistic. ("umi", "rpkm", "tpm" etc.)
#' @slot feature_type .
#' @slot cell_metadata dataframe with metadata on the cells. Rownames correspond to the cell names.
#'
#' @examples
#' # we first initialize a tgScMat object
#' scmat = tgScMat(mat=scrdb::toydata, stat_type="umi")
#'
#' # we next compute gene markers, while also dumping stats into a figure gset.png
#' # this stage likely requires intervention by tuning parameters
#' marks = tgscm_gene_select(scmat)
#'
#' # we next cluster using the knn graph approach and default parameters
#' scl = tgScMatClust(scmat=scmat, feat_mat=scmat@mat[marks,], alg_type="knn_graph")
#' scl_kmeans = tgScMatClust(scmat=scmat, feat_mat=scmat@mat[marks,], alg_type="kmeans")
#'
#' # we then compute the graph/cells 2d layout
#' scl_2d = tgScMatClustLayout2D(scl=scl, alg_type="graph", T_edge=0.08)
#'
#' # optionally you can save the data, clusteirng and layout for later loading (note that the sc mat
#' # will be duplicated - altohugh options to prevent this are available
#' tgscm_export(scl_2d, file="fig/toydata")
#'
#' # plotting the clusters and cells around them - with some default coloring scheme that will
#' # usually won't cut it (unless the model is really small)
#'
#' tgscm_plot_clust_2d(scl_2d)
#'
#' # we can now plot the density map for each marker genes
#' for(nm in marks) {
#'   tgscm_plot_gene_2d(scl_2d,gene_nm=nm, reg_factor=5, w=2000,h=2000,base_dir="fig/")
#' }
#'
#' @importClassesFrom Matrix Matrix dgCMatrix dgeMatrix
#' @import Matrix
#'
#### @export tgScMat
#### @exportClass tgScMat
tgScMat <- setClass(
        "tgScMat",
	slots = c(
	  mat = "dgCMatrix",
	  genes = "vector",
	  cells = "vector",
	  ncells = "numeric",
	  ngenes = "numeric",
	  stat_type = "character",
		feature_type = "character",
	  cell_metadata = "data.frame")
)

setMethod(
  "initialize",
  signature = "tgScMat",
  definition =
    function(.Object, mat=NULL, stat_type = "umi", feature_type = "all", cell_metadata = NULL, ...) {
      .Object@stat_type=stat_type
      .Object@feature_type=feature_type
      # if(!is.null(batch_map)) {
      #    .Object@batch_map = batch_map
      # }
      # mat
      if(!is.null(mat)) {
        if(!is.null(rownames(mat))) {
          .Object@genes=rownames(mat)
        } else {
          .Object@genes=seq(1,nrow(mat),1)
        }
        if(!is.null(colnames(mat))) {
          .Object@cells=colnames(mat)
        } else {
          .Object@cells=seq(1,ncol(mat),1)
        }
      	.Object@ngenes = dim(mat)[1]
      	.Object@ncells = dim(mat)[2]

      	.Object@mat = Matrix(as.matrix(mat))

      	# cell metadata
      	if(!is.null(cell_metadata)) {
      	  .Object@cell_metadata = .process_md(.Object, cell_metadata)
      	}
      }


      if(length(list(...)) > 0) {
        # ...
      }
      return(.Object)
    }
)

# Checks metadata
.process_md = function(.Object, cell_metadata) {
  if(is.null(rownames(cell_metadata))){
    if (dim(cell_metadata)[1]==length(.Object@cells)){
      rownames(cell_metadata) = .Object@cells
    } else {
      stop("Error in tgScMat construction. No cell names supplied for cell_metadata, and the number of cells differ")
    }
  } else {
    # select relevant cells
    if( length(intersect(rownames(cell_metadata), .Object@cells)) ==0) {
      stop("Error in tgScMat construction. Cell names in cell_metadata do not match expression matrix cell names")
    } else {
      if (length(setdiff(.Object@cells, rownames(cell_metadata)))>0) {
        warning("Some cells are missing metadata")
      }
      cell_metadata = cell_metadata[.Object@cells,]
      rownames(cell_metadata) = .Object@cells # in case some are missing
    }

  }
  return (cell_metadata)
}



setValidity("tgScMat", function(object) {
  # TBA
  return(TRUE)
})



# PUBLIC CONSTRUCTORS

#' Constract a tgScMat
#'
#' Constract a tgScMat gene expression matrix object
#'
#'
#' @param mat A matrix containing the expression data (will be stored as a sparse matrix)
#' @param stat_type Type of statistic. ("umi", "rpkm", "tpm" etc.)
#' @param feature_type .
#' @param batch_map batch metadata per cell.
#' @param cell_metadata dataframe with metadata on the cells. Rownames correspond to the cell names.
#'
#' @return A tgScMat gene expression matrix object
#'
#' @export
scm_new_matrix = function(mat, cell_metadata, stat_type = "umi", feature_type = "all") {
	return(tgScMat(as.matrix(mat), stat_type=stat_type, feature_type=feature_type, cell_metadata=cell_metadata))
}


#' Read a matrix from the output of a MARS-Seq run
#'
#' Read a matrix from the output of a MARS-Seq run
#'
#' @param base_dir base directory of MARS pipeline
#' @param mars_batches all mars technical (amplification) batch codes. If null, will read all batches listed in batch_metadata.
#' @param batch_meta_file path to a tab delimited file describing the mars batches. First column must be batch name.
#' Exactly one of \code{batch_meta_file}, \code{batch_meta} should be used.
#'
#' @param batch_meta a data frame describing the mars batches.
#' Exactly one of \code{batch_meta_file}, \code{batch_meta} should be used.
#'
#' @param skip_missing Skip batches that are present in the metadata file but a missing from the UMI directory?
#'
#' @param min_umis_n minimum number of umi's for retaining the cell in the matrix
#'
#' @export
#'
#' @importFrom data.table fread
#'
scm_read_scmat_mars = function(base_dir,
				 mars_batches = NULL,
				 batch_meta_file=NULL,
				 batch_meta = NULL,
				 skip_missing = T,
				 genes_by_first = T,
				 min_umis_n = 200)
{
	batch_meta = .get_md(batch_meta, batch_meta_file, mars_batches)

	if (is.null(mars_batches)) {
		mars_batches = batch_meta[,1]
	} else {
		# check
		dif = setdiff(mars_batches, batch_meta[,1])
		if (length(dif) > 0) {
			stop ("Metadata missing for batches ", paste(dif, collapse=" "))
		}
	}
	# umis$Row.names = rownames(umis) done in fread_rownames
	cells_in_batch = c()
	good_batches = c()
	for(i in 1:length(mars_batches)) {
		batch = as.character(mars_batches)[i]
		fn = sprintf("%s/%s.txt", base_dir, batch)
		if(file.exists(fn) | !skip_missing) {
			good_batches = c(good_batches, batch)
			bumis = fread_rownames(sprintf("%s/%s.txt", base_dir, batch), sep="\t")
			if(length(cells_in_batch)==0) {
				umis = bumis
				cells_in_batch = c(ncol(umis) - 1)
			} else {
				if(genes_by_first) {
					if(nrow(umis) != nrow(bumis)) {
						message("row mismatch in loading mars batch ", batch, " reverting to join")
						umis = umis %>% left_join(bumis)
					} else {
						umis = cbind(umis,bumis[,-1])
					}
				} else {
					umis = umis %>% left_join(bumis)
				}
				cells_in_batch = c(cells_in_batch, ncol(bumis) - 1)
			}
			cat ("read", batch, ",dim ", paste(dim(umis), collapse=" "), "\n")
			rownames(umis) = umis$rowname
		} else {
			cat ("skip missing ", batch)
		}
	}
	no_ercc = grep("ERCC", rownames(umis), invert=T)
	umis = umis[no_ercc, -1] # remove ERCC & first col
	umis = umis[rowSums(umis)>1,]

	rownames(batch_meta) = batch_meta[,1]
	cell_metadata = batch_meta[rep(as.character(good_batches), times = cells_in_batch),]

	# filter small cells
	f = colSums(umis)>min_umis_n
	umis = umis[,f]
	cell_metadata = cell_metadata[f,]
	cat("Filtered", sum(!f), "small cells\n")

	rownames(cell_metadata) = colnames(umis)

	return(tgScMat(as.matrix(umis), stat_type="umi", cell_metadata = cell_metadata))
}


# get metadata
.get_md = function(batch_meta, batch_meta_file, mars_batches) {
	if (is.null(batch_meta) & is.null(batch_meta_file)) {
		stop("Batch metadata is not specified")
	}
	if (!is.null(batch_meta) & !is.null(batch_meta_file)) {
		stop("Exactly one of batch_meta_file, batch_meta should be used.")
	}
	if (!is.null(batch_meta_file)) {
		batch_meta = read.delim(batch_meta_file)
	}

	batch_meta = as.data.frame(unclass(batch_meta)) # convert char to levels
	return (batch_meta)
}


#' Extract sub-matrix
#'
#' Extract sub-matrix
#'
#' @param scmat A tgScMat object.
#' @param genes Genes range.
#' @param cells Cells range
#'
#' @export
scm_sub_mat = function(scmat, genes=NULL, cells=NULL) {
	if (class(scmat)[1] != "tgScMat") {
		stop("invalid scmat in scm_sub_mat")
	}
	if (is.null(genes)) {
		genes = scmat@genes
	}
	if (is.null(cells)) {
		cells = scmat@cells
	}

	if(length(cells)<2) {
		stop("At least 2 cells must be selected")
	}
	if(length(genes)<2) {
		stop("At least 2 genes must be selected")
	}
	meta = scmat@cell_metadata[cells,]
	# remove unused levels
	meta[] <- lapply(meta, function(x) if(is.factor(x)) factor(x) else x)
	return(tgScMat(mat = scmat@mat[genes, cells,drop=F], stat_type= scmat@stat_type, feature_type = scmat@feature_type,
								 cell_metadata= meta))
	#batch_map = scmat@batch_map[cells],

}


#' Read a tgScMat object that saved to disk.
#'
#' @param file Name of the file for inputting.
#'
#' @export
#'
scm_import_mat = function (file) {
	#mat = read.table(file=sprintf("%s.scmat", file), head=T, sep="\t")
	mat = fread_rownames(file=sprintf("%s.scmat", file), sep="\t", set_rownames = T)

	attr = as.list(read.table(file=sprintf("%s.scmat_attr", file), sep="\t", stringsAsFactors = F))
	# cell_metadata = read.table(file=sprintf("%s.scmat_cell_metadata", file), head=T, sep="\t")
	cell_metadata = fread_rownames(file=sprintf("%s.scmat_cell_metadata", file), sep="\t", set_rownames = T)

	if(is.null(attr$stat_type)) {
		attr$stat_type = "undef"
	}
	if(is.null(attr$feature_type)) {
		attr$feature_type = "undef"
	}
	return(tgScMat(as.matrix(mat), stat_type=attr$stat_type, feature_type=attr$feature_type, cell_metadata=cell_metadata))
}


####

#'
#' Export a matrix to file.
#'
#' @param file Prefix of file names for outputting.
#'
#' @export
#'
#'
#'
setGeneric("tgscm_export",
           function(.Object, file,...) stnadrdGeneric("tgscm_export"))
setMethod(
  "tgscm_export",
  signature = "tgScMat",
  definition =
    function(.Object, file, ...) {
      write.table(as.matrix(.Object@mat), file=sprintf("%s.scmat", file), quote=F, sep="\t")
      scmat_attr = list(stat_type = .Object@stat_type, feature_type = .Object@feature_type)
      write.table(scmat_attr, file=sprintf("%s.scmat_attr", file), quote=F, sep="\t")
      # metadata
      write.table(as.matrix(.Object@cell_metadata), file=sprintf("%s.scmat_cell_metadata", file), quote=F, sep="\t")

    }
)



#' Extract sub-matrix
#'
#' Extract sub-matrix
#'
#' @param scmat A tgScMat object.
#' @param epsilon: ambient noise upper-bound
#'
#' @export
scm_remove_ambient_by_epsilon = function(scmat, epsilon, metadata_batch, min_umi_n = -1)
{
	#iter on batches
	#epsilon*U/N = something. the old heuristc is to remove below T as long as sum(ui<T) < epsilon_upper*U.The problem is that it creates.
	us = scmat@mat
	batch_factor = scmat@cell_metadata[,metadata_batch]
	for(b in unique(batch_factor)) {
		message("cleaning ", b)
		cells = which(batch_factor == b)
		ncell = length(cells)
		tot_g = rowSums(us[,cells])
		thresh_g = tot_g*epsilon/ncell
		thresh_u = us[,cells] < thresh_g & us[,cells] > 0
		us[,cells][thresh_u] = 0
	}
	scmat@mat = us
	if(min_umi_n > 0) {
		scmat = scm_sub_mat(scmat, scmat@genes, names(which(colSums(us)>min_umi_n)))
	}
	return(scmat)
}

####

setMethod(
	"show",
	signature = "tgScMat",
	definition =
		function(object) {
			cat("An object of class ", class(object), ",", sep = "")
			cat(" stat type ", object@stat_type, ", feat select ", object@feature_type, ".\n", sep = "")
			cat(length(object@cells), " cells by ", nrow(object@mat), " genes. median cell content ", median(colSums(object@mat)), ".\n", sep = "")
			invisible(NULL)
		}
)

