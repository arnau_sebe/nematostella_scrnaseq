#' A clustered matrix
#
#' Representing a clustered matrix, and implementing several basic algorithm for constructing it
#' from a matrix
#'
#' @slot scmat tgScMat.
#' @slot feat_mat Matrix.
#' @slot clusts vector.
#' @slot clust_fp matrix.
#' @slot alg_params list.
#' @slot knn_ordered  k_knn X ncells matrix,  column i contains cell i's k nearest neighbors, ordered fronm closest to farthest.
#'
#' @importClassesFrom Matrix dgCMatrix
#'
#### @export tgScMatClust
#### @exportClass tgScMatClust
#
#### @include scmat.r
tgScMatClust <- setClass(
  "tgScMatClust",
  slots = c(
    scmat = "tgScMat",
    feat_mat = "Matrix",
    clusts = "vector",
    clust_fp = "matrix",
    clust_gcov = "matrix",
    alg_params = "list",
    nclust = "integer",
    knn_ordered = "matrix",
    rseed = "numeric")
)

.scc_init = function(scmat, features = NULL, alg_type, rseed = 1,
										 factor_by_median = F,
										 norm_size = F, k_scale_umi = 7) {

	scc = new("tgScMatClust")
	scc@scmat = scmat

	scc@feat_mat = create_feature_mat(scmat, features, factor_by_median = factor_by_median,
																		norm_size = norm_size, k_scale_umi = k_scale_umi)

	scc@alg_params = list(type=alg_type, rseed = rseed) # this list is extended after clustering

	return(scc)
}


.scc_postprocess = function(scc) {
	message("will comp fp")
	scc@clust_fp = .calc_clusts_fp(scc)
	message("will comp gov")
	scc@clust_gcov = .row_stats_by_factor(scc@scmat@mat > 0, fact = scc@clusts, rowFunction = rowMeans)
	scc@nclust = length(unique(scc@clusts))

	return(scc)
}

#' Cluster single cell data
#'
#' Use one of the avialable clustering methods
#'
#' @param scmat A tgScMat expression matrix.
#' @param features Either  a matrix of cell features (gene expression, PCA principle componant, etc.),
#' or a list of markers to be extracted from scmat. Clustering will be performed on these features.
#' If left blank, markers will be selected using the default method.
#'
#' @param alg_type One of: knn, kmeans.
#'
#' @return A tgScMatClust object
#'
#' @export
scc_cluster = function(scmat, features = NULL, alg_type = "knn", ...) {

	if(alg_type == "kmeans") {
		.Object = scc_cluster_kmeans(scmat, features, ...)
	} else if(alg_type == "knn") {
		.Object = scc_cluster_knn_graph(scmat, features, ...)
	} else if(alg_type == "hclust") {
		.Object = scc_cluster_hclust(scmat, features, ...)
	# } else if(alg_type == "multinom_em") {
	# 	.Obect = scc_cluster_multinom_em(scmat, features, ...)
	} else {
		stop("algorithm ", alg_type, " is not recognized")
	}
}

#' Cluster single cell data using kmeans
#'
#' Cluster single cell data using kmeans
#'
#' @param scmat A tgScMat expression matrix.
#' @param features Either  a matrix of cell features (gene expression, PCA principle componant, etc.),
#' or a list of markers to be extracted from scmat. Clustering will be performed on these features.
#' If left blank, markers will be selected using the default method.
#'
#' @param K number of clusters
#'
#'
#' @return A tgScMatClust object
#'
#' @export
scc_cluster_kmeans = function(scmat, features = NULL, K=NA, k_scale_umi = 7,
															factor_by_median = T, norm_size=T, rseed = 1)
{
	oldseed = .set_seed(rseed)
	scm_cl = .scc_init(scmat, features, alg_type = "kmeans", factor_by_median = factor_by_median,
										 norm_size = norm_size, k_scale_umi = k_scale_umi)

	if(is.na(K)) {
		K = max(2,ceiling(ncol(scm_cl@feat_mat)/60))
	}
	cat("Running kmeans with K =", K, "...")

	# n_median = 1
	# if(factor_by_median == T) {
	# 	n_median = median(colSums(us))
	# 	# cat("..use median norm ", n_median)
	# }
	# if(norm_size) {
	# 	# cat(", norm size ")
	# 	us = n_median * t(t(us)/(1+colSums(us)))
	# }
	# if(scm_cl@scmat@stat_type == "umi") {
	# 	if(is.na(k_scale_umi)) {
	# 		#find a better heuristic
	# 		k_scale_umi = 7
	# 	}
	# 	cell_fps = t(log2(1+k_scale_umi*us));
	# } else {
	# 	if(min(us) > 0 & max(us)/min(us)>100) {
	# 		cell_fps = t(log2(us))
	# 	} else {
	# 		cell_fps = us
	# 	}
	# }

	km = TGL.kmeans2(as.matrix(t(scm_cl@feat_mat)), K)
	scm_cl@clusts = km$cluster
	scm_cl@alg_params =  c(scm_cl@alg_params, list (K=K, k_scale_umi = k_scale_umi,
																									factor_by_median = factor_by_median, norm_size = norm_size))

	scm_cl = .scc_postprocess(scm_cl)
	.restore_seed(oldseed)

	return(scm_cl)
}

#' Cluster single cell data using KNN graph
#'
#' Cluster single cell data using KNN graph
#'
#' @param scmat A tgScMat expression matrix.
#' @param features Either  a matrix of cell features (gene expression, PCA principle componant, etc.),
#' or a list of markers to be extracted from scmat. Clustering will be performed on these features.
#' If left blank, markers will be selected using the default method.
#'
#' @param K number of clusters
#'
#'
#' @return A tgScMatClust object
#'
#' @export
scc_cluster_knn_graph = function(scmat, features = NULL,
		k_scale_umi = 7, min_clust_size = NA,
		use_balanced_knn = T,
		k_knn=NA, do_consolidation=T, rseed = 1)
{
	oldseed = .set_seed(rseed)
	scm_cl = .scc_init(scmat, features, alg_type = "knn", factor_by_median = F,
										 norm_size = F, k_scale_umi = k_scale_umi)

	#TBA
	ratio = 0.5

	if(is.na(k_knn)) {
		# ??
		k_knn = min(75, scm_cl@scmat@ncells / 10)
	}
	if(is.na(min_clust_size)) {
		min_clust_size = min(floor(ratio * k_knn), 10)
	}

	if (min_clust_size > ratio * k_knn) {
		min_clust_size = floor(ratio * k_knn)
		cat("reducing minimal cluster size to ", min_clust_size)
	}
	cat("Running knn clust with K =", k_knn,"...\n")

	#must filter low complexity cells? of detect by knn?

	cells = scm_cl@scmat@cells
	ncells = length(cells)

	if(ncells < k_knn*2) {
		stop("K ", k_knn, " for clustering is too large given only ", ncells, " cells")
	}

	if(use_balanced_knn) {
		m_knn = .scc_comp_balanced_knn_matrix(scm_cl, k_knn)
	} else {
		m_knn = .scc_comp_knn_matrix(scm_cl, k_knn)
	}

	m_knn = m_knn / k_knn

	clusts = .tg_knn_clust(m_knn, min_clust_size=min_clust_size, consolidate=do_consolidation)

	clusts1 = as.integer(as.factor(clusts))
	names(clusts1) = names(clusts)
	clusts= clusts1
	message("will reorder")
	clusts = .reorder_knn_clusts(scm_cl@feat_mat, clusts)

	scm_cl@clusts = clusts
	scm_cl@alg_params = c(scm_cl@alg_params, list(k_knn = k_knn, k_scale_umi = k_scale_umi,
				min_clust_size = min_clust_size, do_consolidation = do_consolidation))

	message("will postproc clusts")
	scm_cl = .scc_postprocess(scm_cl)
	.restore_seed(oldseed)

	return(scm_cl)
}

scc_cluster_hclust = function(scmat, features, K=NA)
{
	#TBA
	stop("ERROR: hclust not supported yet.")
	return(NA)
}

#
#' Extract features mat
#'
#' @param scmat A tgScMat expression matrix.
#' @param features Either  a matrix of cell features (gene expression, PCA principle componant, etc.),
#' or a list of markers to be extracted from scmat. Clustering will be performed on these features.
#' If left blank, markers will be selected using the default method.
#'
#' @return A matrix with one row per marker gene, one column per cell.
#' Gene expression is normalized according to the parameters.
#'
#' @export
create_feature_mat = function(scmat, features = NULL,
															factor_by_median = F, norm_size = F,
															k_scale_umi = 7) {

	feat_mat = .extract_feat_mat(scmat, features)
	feat_mat = .normalize_feat(scmat, feat_mat,
														 factor_by_median = factor_by_median, norm_size = norm_size,
														 k_scale_umi = k_scale_umi)

	return(Matrix(feat_mat))
}

#
# Normalize
#
.normalize_feat = function(scmat, feat_mat,
				factor_by_median = F,
				norm_size = F, k_scale_umi = 7)
{
	umis = feat_mat
	n_median = 1
	if(factor_by_median) {
		n_median = median(colSums(umis))
		# cat("..use median norm ", n_median)
	}
	if(norm_size) {
		# cat(", norm size ")
		umis = n_median * t(t(umis)/(1+colSums(umis)))
	}
	if(scmat@stat_type == "umi") {
		# if(is.na(k_scale_umi)) {
		# 	#find a better heuristic
		# 	k_scale_umi = 7
		# }
		cell_fps = log2(1+k_scale_umi*umis)
	} else {
		if(min(umis) > 0 & max(umis)/min(umis)>100) {
			cell_fps = log2(umis)
		} else {
			cell_fps = umis
		}
	}
	return(cell_fps)
}

.extract_feat_mat = function(scmat, features = NULL){

	#
	# extract expression for markers
	#
	if (is.null(features)) {
		# select features
		cat("Extracting markers using default parameters.\n")
		feat_mat = as.matrix(scmat@mat[select_markers(scmat),])

	} else if (is.null(dim(features)) && length(intersect(features, scmat@genes) > 0)) {

		# got markers
		marks = intersect(features, scmat@genes)
		if (length(marks) != length(features)) {
			warning ("Used only ", length(marks), " out of ", length(features) , " features.")
		}
		feat_mat = as.matrix(scmat@mat[marks,])
	} else if (length(dim(features)) == 2 && dim(features)[2] == scmat@ncells) {

		# got 2D feature matrix
		feat_mat = as.matrix(features)
	} else {
		stop("invalid features used for clustering")
	}
	return(feat_mat)
}

####


#' @export
#'
setMethod(
	"show",
	signature = "tgScMatClust",
	definition =
	 function(object) {
		cat("tgScMatClust, ", length(object@clusts), " cells on ",
				length(unique(object@clusts)), " clusts\n")
	 	cat("Parameters: ")
	 	print(object@alg_params)
		invisible(NULL)
	 }
)

#'
#' Export a clustering results to file.
#'
#' @param file Prefix of file names for outputting.
#'
#' @export
#'
# setGeneric("tgscm_export",
	# function(.Object, file,...) stnadrdGeneric("tgscm_export"))
setMethod(
	"tgscm_export",
	signature = "tgScMatClust",
	definition =
	 function(.Object, file, supress_mat=F, ...) {
		if(!supress_mat) {
		  tgscm_export(.Object@scmat, file)
		}
    write.table(as.matrix(.Object@feat_mat), file=sprintf("%s.feat", file), quote=F, sep="\t")
		write.table(.Object@clusts, file=sprintf("%s.clust", file), quote=F, sep="\t")
		write.table(.Object@clust_fp, file=sprintf("%s.clust_fp", file), quote=F, sep="\t")
		write.table(.Object@alg_params, file=sprintf("%s.alg", file), quote=F, sep="\t")
	 }
)

#' Read a clustering result from file.
#'
#' @param file Name of the file for inputting.
#'
#' @param scmat an instance of the relevant \code{\linkS4class{tgScMat}}, optional.
#'
#' @export
#'
#'
scc_import_clust = function(file, scmat=NULL) {
	if(is.null(scmat)) {
		scmat = scm_import_mat(file)
	}
  .Object = tgScMatClust()
	.Object@scmat = scmat
	.Object@feat_mat = Matrix(as.matrix(read.table(file=sprintf("%s.feat", file), head=T, sep="\t")))
	.Object@clusts = read.table(file=sprintf("%s.clust", file), sep="\t")
	.Object@clusts = data.matrix(.Object@clusts)[,1] # convert to array
	.Object@clust_fp = as.matrix(read.table(file=sprintf("%s.clust_fp", file), sep="\t", check.names=F))
	.Object@alg_params = as.list(read.table(file=sprintf("%s.alg", file), sep="\t", stringsAsFactors = F))
	.Object@nclust = length(unique(.Object@clusts))
	return(.Object)
}


# create a footprint
.calc_clusts_fp = function(scm_cl, ref_func = median)
{
	us = scm_cl@scmat@mat
	f_g_cov = rowSums(us) > 10

	clust_geomean = .row_stats_by_factor(us[f_g_cov,], scm_cl@clusts, function(y) {exp(rowMeans(log(1+y)))-1})
	# clust_geomean =  t(apply(us[f_g_cov,], 1,  #slow!!
	# 		function(x) tapply(x,
	# 			scm_cl@clusts,
	# 			function(y) exp(mean(log(1+y)))-1)))
	clust_meansize = tapply(colSums(us), scm_cl@clusts, mean)
	ideal_cell_size = pmin(1000, median(clust_meansize))
	g_fp = t(ideal_cell_size*t(clust_geomean)/as.vector(clust_meansize))
	#normalize each gene
	fp_reg = 0.1   #0.1 is defined here because 0.1*mean_num_of_cells_in_cluster 
		       #is epxected to be 3-7, which means that we regulairze 
		       #umicount in the cluster by 3-7.
	g_fp_n = (fp_reg+g_fp)/apply(fp_reg+g_fp, 1, ref_func)

	return(g_fp_n)
}

.reorder_knn_clusts = function(data, clusts) {
	k = length(unique(clusts))
	# # reorder clusters
	centers = .row_stats_by_factor(data, clusts)
	centers_hc = hclust(dist(cor(centers)), "ward.D2")
	tomap = rep(NA, k)
	tomap[centers_hc$order] = 1:k
	onames = names(clusts)
	clusts = tomap[clusts]
	names(clusts) = onames
	return(clusts)
}




# return a k_knn X ncells matrix,  column i contains cell i's k nearest neighbors,
# ordered fronm closest to farthest.
# IMPORTANT: calculated matrix might have more rows than expected! always use only the first relevant rows,
# if k is small, will calculate more than requested, since the knn object is used in more than 1 place.
.scc_calc_ordered_knn = function(scl, k_knn = -1)
{
	message("Calling calc order knn in cluster.r")

	minimal_k = sqrt(length(scl@scmat@cells))

	if(length(scl@knn_ordered) > 1 && # knn_ordered exists
		 nrow(scl@knn_ordered) >= k_knn) {
		# no need to calculate again!
		return(scl)
	}

	ncells = length(scl@scmat@cells)
	if (k_knn == -1)  {
		k_knn = max(30, sqrt(ncells))
	}
	if(k_knn < minimal_k) {
		k_knn = minimal_k
	}
	sim_mat = .calc_sim_mat(scl)

	m_knn_ordered = matrix(NA, nrow = k_knn, ncol = ncells)
	# m_knn = matrix(0,nrow = ncells,ncol = ncells, dimnames = list(colnames(sim_mat), colnames(sim_mat)))

	# ranks = rev(1 - (0:k_knn+1)/(k_knn + 2))
	for(cell in 1:ncells) {
		knn = head(order(sim_mat[cell,], decreasing = T),n=k_knn+1)
		m_knn_ordered[,cell] = knn[-1] # remove self
		# m_knn[knn, cell] = ranks
	}
	scl@knn_ordered = m_knn_ordered
	return(scl)
}

.scc_comp_knn_matrix = function(scl, k_knn)
{
# calc knn_ordered and save in object
	cells = scl@scmat@cells
	ncells = length(scl@scmat@cells)

	scm_cl = .scc_calc_ordered_knn(scm_cl, k_knn)
	# extract m_knn
	m_knn = matrix(0,nrow = ncells,ncol = ncells, dimnames = list(cells, cells))
	ranks = (1 - (0:k_knn+1)/(k_knn + 2))[-1]
	for(cell in 1:ncells) {
		m_knn[scm_cl@knn_ordered[1:k_knn,cell], cell] = ranks
	}

	message("done init m_knn")
	return(m_knn)
}

.scc_comp_balanced_knn_matrix = function(scl, k_knn, k_expand=2)
{
	cells = scl@scmat@cells
	ncells = length(scl@scmat@cells)
	k_knn10 = min(k_knn*10, ncells-1)
	scl = .scc_calc_ordered_knn(scl, k_knn10)

	m_knn = matrix(k_knn10, nrow = ncells,ncol = ncells, dimnames = list(cells, cells))
	for(cell in 1:ncells) {
		m_knn[scl@knn_ordered[1:k_knn10,cell], cell] = 1:k_knn10
	}
	M = k_knn*k_knn*k_expand
	m_knn_io = pmax(-m_knn * t(m_knn) + M,0)

	A = nrow(m_knn_io)-k_knn*3	#no more than k_nn*3 contribs per row, or incoming neighbots
	m_knn_io_rows = t(apply(m_knn_io, 1, function(x) pmax(rank(x)-A,0)))
	A = nrow(m_knn_io)-k_knn	#no more than k_nn contribs per column, or outgoing neighbors
	m_knn_io = apply(m_knn_io_rows, 2, function(x) pmax(rank(x)-A,0))

	return(m_knn_io)
}


.calc_sim_mat = function(scl) {

	# this should be extracted
	# TEMP! log and scaling should be done before clustering

	# if(scl@scmat@stat_type == "umi") {
	# 	if(is.na(k_scale_umi)) {
	# 		#find a better heuristic
	# 		k_scale_umi = 7
	# 	}
	# 	# message("transforming umis")
	# 	d = as.matrix(log2(1+k_scale_umi*scl@feat_mat))
	# } else {
	# 	us = scl@feat_mat
	# 	if(min(us) > 0 & max(us)/min(us)>100) {
	# 		d = t(log2(us))
	# 	} else {
	# 		d = us
	# 	}
	# }

	message("Will compute cor on feat mat")
	return(as.matrix(cor(as.matrix(scl@feat_mat))))
	message("done compute cor on feat mat")
}

scc_filter_bad_clusters = function(scl, 
				min_max_fold_change=NA, 
				confu = NULL,
				k_knn_for_confu = NA,
				min_self_confusion = NA)
{
	if(is.na(min_max_fold_change) & is.na(min_self_confusion)) {
		stop("at least one threshold (fold change or self confusion) need to be determined in scc_filter_bad_clusters")
	}
	if(!is.na(min_self_confusion) & is.null(confu)) {
		if(is.na(k_knn_for_confu)) {
			stop("in scc_filter_bad_clusters, either provide a confusion matrix or the k parameter for computing it, or avoid setting the confusion threshold")
		}
		mknnio_ij = .scc_comp_balanced_knn_matrix(scl, k_knn_for_confu, 10)
		isclust_ci = diag(max(scl@clusts))[,scl@clusts]
		confu =isclust_ci %*% (mknnio_ij>0) %*% t(isclust_ci)
	}
	max_fc = apply(scl@clust_fp,2, max)

	bad_clusts = c()
	if(!is.na(min_max_fold_change)) {
		bad_clusts = which(max_fc < min_max_fold_change)
		message("removing ", length(bad_clusts), " clusters due to low fold change in their gene expression")
	}
	if(!is.na(min_self_confusion)) {
		self_confu = diag(confu)/rowSums(confu)
		bad_clusts1 = which(self_confu < min_self_confusion)
		bad_clusts = union(bad_clusts, bad_clusts1)
		message("removing ", length(bad_clusts1), " clusters due to low self confusion, and in total ditching ", length(bad_clusts), " clusters")
	}
	if(length(bad_clusts) == 0) {
		return(scl)
	}
	bad_cells = names(scl@clusts)[which(scl@clusts %in% bad_clusts)]

	good_cells = setdiff(colnames(scl@scmat@mat), bad_cells)

	scl@scmat = scm_sub_mat(scl@scmat, cells=good_cells)
	scl@feat_mat = scl@feat_mat[,good_cells]
	scl@clusts = as.integer(as.factor(scl@clusts[good_cells]));names(scl@clusts)=good_cells
	scl@clusts = .reorder_knn_clusts(scl@feat_mat, scl@clusts)
	scl = .scc_postprocess(scl)
	scl@nclust = scl@nclust - length(bad_clusts)
        if(length(scl@knn_ordered) > 1) {
		scl@knn_ordered = scl@knn_ordered[good_cells,]
	}
	return(scl)
}

find_clustering_outliers = function(scl, T_lfc = log2(10), min_outlier_u = 8)
{
	# 1 encoding of the clusters, rows= clusters, cols = cells
	isclust_ci = diag(max(scl@clusts))[,scl@clusts]
	#we compute the expected number of umi per gene per cell given clust
	u_gi = scl@scmat@mat

	u_i = colSums(as.matrix(u_gi))

	ishigh_g = apply(u_gi, 1, max)>=min_outlier_u

	u_gi = u_gi[ishigh_g,]

	u_gc = as.matrix(u_gi %*% t(isclust_ci))
	u_c = colSums(u_gc)

	p_gc = t(t(u_gc) / u_c)
	p_gi = p_gc %*% isclust_ci
	exp_gi = t(t(p_gi) * u_i)

	#we estimate a premissive sd on that
	lfc_gi = log2((1+u_gi)/(1+exp_gi))

#	sd_gi = pmax(sqrt(ceiling(exp_gi)),1)
#	z_gi = (u_gi - exp_gi)/sd_gi

	maxlfc_g = apply(lfc_gi, 1, max)
	maxlfc_i = apply(lfc_gi, 2, max)

	if(sum(maxlfc_g>T_lfc) > 1 & sum(maxlfc_i > T_lfc) > 2) {
		outu_gi = as.matrix(u_gi[maxlfc_g>T_lfc, maxlfc_i > T_lfc])

	#reporting the outlier gene / cell matrix

		hc1 = hclust(dist(cor(outu_gi)), "ward.D2")
		hc2 = hclust(dist(cor(t(outu_gi))), "ward.D2")
		png("outlier_mat.png", w=2000, h=2000)
		shades = colorRampPalette(c("white", "blue", "red", "yellow", "black"))(1000)
		image(t(log2(1+outu_gi[hc2$order, hc1$order])), col=shades, xaxt='n', yaxt='n')
		mtext(rownames(outu_gi)[hc2$order], at=seq(0,1,l=length(hc2$order)), las=2, side=2, cex=0.8)
		mtext(rownames(outu_gi)[hc2$order], at=seq(0,1,l=length(hc2$order)), las=2, side=4, cex=0.8)
		dev.off()
	}

	return(names(which(maxlfc_i > T_lfc)))
}

.scc_clean_ambient_on_clusts = function(scl, orig_mat, epsilon, batch_attr, T_zclean=2)
{
	message("will clean ambient on clusts")
	batch_factor = as.integer(as.factor(scl@scmat@cell_metadata[names(scl@clusts),batch_attr]))

	isclust_ci = diag(max(scl@clusts))[,scl@clusts]    # 1 encoding of the clusters, rows= clusters, cols = cells
	isbatch_bi = diag(max(batch_factor))[,batch_factor]    # 1 encoding of the clusters, rows= clusters, cols =nodes
	#we break clusts on batches
	n_cb = table(scl@clusts, batch_factor)
	n_b = colSums(n_cb)
	n_c = as.vector(table(scl@clusts))
	n = sum(n_c)

	#we compute total U_{g,b} - number of umis per gene per batch
	u_gi = orig_mat[,names(scl@clusts)]

#	u_gb = t(apply(u_gi, 1, function(x) tapply(x, batch_factor, sum)))
	u_gb = u_gi %*% t(isbatch_bi)
	amb_gb = t(t(u_gb) * (epsilon/n_b))

#f_g just to save time and
	f_g = apply(amb_gb, 1, max) > 0.02

	u_gi = u_gi[f_g,]
	u_gb = u_gb[f_g,]
	amb_gb = amb_gb[f_g,]

	Tnotamb_gb = ceiling(ceiling(amb_gb)+3*sqrt(ceiling(amb_gb)))
	Tnotamb_gi = Tnotamb_gb[,batch_factor]
	umaxamb_gi = pmin(as.matrix(u_gi), as.matrix(Tnotamb_gi))
#	Tnotamb_f = u_gi > Tnotamb_gi

	#we compute amb_{g,c} and obs_{g,c} as the expected number of
	#ambient umi's per cluster, and the observed number
	#the observed number is scissoring high umi count cells to avoid
	#outlier take over
	amb_gc = as.matrix(amb_gb) %*% t(as.matrix(n_cb))

	obs_gc = umaxamb_gi %*% t(isclust_ci)
#	obs_gc = t(apply(umaxamb_gi, 1, function(x) tapply(x, scl@clusts, sum)))

	d_gc = obs_gc - amb_gc
	z_gc = d_gc / sqrt(amb_gc)

#	save(Tnotamb_gb, Tnotamb_gi, umaxamb_gi, amb_gc, obs_gc, d_gc, z_gc, "dbg_amb.Rda")

#filtered genes are those for which:
# clusters on z_gc>T_Z capture more than 1-k*epsilon of the umi
# at least 1/4 of the cells are in cells with Z<2
	u_gc = u_gi %*% t(isclust_ci)
#	u_gc =  t(apply(u_gi, 1, function(x) tapply(x, scl@clusts, sum)))
	upos_gc = u_gc * (z_gc<T_zclean)
	ambumitot_g = rowSums(upos_gc)/rowSums(u_gc)
	ambcelltot_g = (z_gc<T_zclean) %*% as.matrix(n_c)

	tofilt_g = (ambcelltot_g > (n/8)) & (ambumitot_g < epsilon*2)

# for these case we clean umis below Tnotamb_gi in clusters with Z<1
	z_gi = z_gc[,scl@clusts]
	z_gi[!tofilt_g,] = 8
	cleanu_gi = u_gi
	cleanu_gi[z_gi<T_zclean & u_gi < Tnotamb_gi] = 0
	full_gi = orig_mat[,names(scl@clusts)]
	full_gi[f_g,] = cleanu_gi
	save(full_gi,
		u_gi,
		cleanu_gi,
		z_gi,
		tofilt_g,
		ambcelltot_g,
		ambumitot_g,
		upos_gc,
		u_gc, z_gc, d_gc,
		amb_gc, obs_gc,
		Tnotamb_gb, Tnotamb_gi, umaxamb_gi, orig_mat,
		u_gb,
		file="dbg_celanamb.Rda")

	return(full_gi)
}

.scc_find_batchy_genes_on_clusts = function(scl, orig_mat, batch_attr, n_reg = 10)
{
	message("will search for batchy genes on clusters")
	batch_factor = as.integer(as.factor(scl@scmat@cell_metadata[names(scl@clusts),batch_attr]))

	isclust_ci = diag(max(scl@clusts))[,scl@clusts]    # 1 encoding of the clusters, rows= clusters, cols = cells
	isbatch_bi = diag(max(batch_factor))[,batch_factor]    # 1 encoding of the batch, rows= batch, cols =nodes

#number of cells in cluster,batch
	n_cb = table(scl@clusts, batch_factor)
	n_b = colSums(n_cb)
	n_c = as.vector(table(scl@clusts))
	n = sum(n_c)

	u_gi = orig_mat[,names(scl@clusts)]

#now the 
#we compute total U_{g,b} - number of umis per gene per batch
	n_gb = u_gi %*% t(isbatch_bi)

	n_gc = as.matrix(u_gi %*% t(isclust_ci))
#now we need to find the expected number of umi per gene and batch e_gb\	
	e_gb = n_gc %*% (as.matrix(n_cb)/n_c)
	return(list(ratio=(n_gb+n_reg)/(e_gb+n_reg), e=e_gb, o=n_gb))
}

# # Not so fast pearson correlation for sparse column matrices
# .fastcor = function(x #sparse
# 										){
# 	n = nrow(x)
# 	a = crossprod(x) # must be done on the sparse matrix for performance
# 	means = sqrt(n) * colMeans(x)
# 	stds = sqrt(colSums(x*x) - means^2)
# 	a = (a - tcrossprod(means)) / tcrossprod(stds)
# 	return (a)
# }
