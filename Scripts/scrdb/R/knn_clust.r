#' Generic K-nn graph clustering
#
#'
#' @param scm_cl empty tgScMatClust object
#' @param k_knn the number of neighbors to be considered
#' @param min_clust_size  used to filter small clusters at the end of the process
#'
#' @importClassesFrom Matrix Matrix
#'
.tg_knn_clust = function(m_knn, min_clust_size, verbose = F, consolidate=F)
{
	nodes = rownames(m_knn)
	n_nodes = ncol(m_knn)
#	bad_knn = rowSums(m_knn>0)<=3 #todo - we want to define these as outliers?

	m_knn = Matrix(m_knn, sparse=T)

	m_knn_01 = m_knn>0
	m_knn_01 = Matrix(m_knn_01, sparse=T)

#now each entry represent the total weight of going
	m_knn2 = t(m_knn) %*% m_knn

	m_knn2 = Matrix(m_knn2, sparse=T)

#outgoing mat will store the links from clustering seeds to their neighbors
	outgoing_mat = Matrix(0,
			nrow = nrow(m_knn), ncol=ncol(m_knn), sparse=T)


	rownames(outgoing_mat) = nodes
	colnames(outgoing_mat) = nodes
	outgoing_weight_d1 = rep(0,n_nodes)
	outgoing_weight_d2 = rep(0,n_nodes)
	outgoing_weight_d3 = rep(0,n_nodes)

	incoming_weight_d1 = rep(0,n_nodes)

	if(verbose) {
	      message("start seeding iterations")
	}
#we sample the next seed from those that are
#1. not yet reachable (d1==0)
#2. preferebly not directly adjacent to reachable (d2==0)
#3. preferebly are at distance 3 (d3>0)
# this is intended to enhance packing of the knn graph by the clusters

#outging_mat - G_i - a graph containing only outgoing edges from seeds
#N(seeds) - all nodes that are adjacent to seeds. (v s.t. deg_{G_i}(v)>0\}
#outgoing weight_d1 - indegree(G_i)
#uncovered neigh[i] - |N(i) \ N(seeds)|
#outgoing_weight_d2[j] - number of paths of the form seed->v<-j in the entire graph
#outgoing_weight_d3[j] - number of paths of the form seed->v<-u<-j in the entire graph
#

	seeds = c()
	uncovered_neigh = colSums(m_knn_01)
	cands_gap = which(outgoing_weight_d2 == 0)
	while(length(cands_gap) > 0) {
		seed = cands_gap[sample(length(cands_gap), 1,
				prob=(1e-10+outgoing_weight_d3[cands_gap]))]
		if(verbose) {
		      message("sample gapped seed, ", seed)
		}

		seeds = c(seeds, seed)
		outgoing_mat[seed,] = m_knn[,seed]

		new_covs = (outgoing_weight_d1 == 0) & (outgoing_mat[seed,] > 0)
		outgoing_weight_d1 = outgoing_weight_d1 + outgoing_mat[seed,]  #colSums(outgoing_mat)
		if(verbose) { message("new covs ", sum(new_covs)) }
		uncovered_neigh = uncovered_neigh - new_covs %*% m_knn_01

		outgoing_weight_d2 = colSums(outgoing_mat %*% m_knn)
		outgoing_weight_d3 = colSums(outgoing_mat %*% m_knn2)
		if (verbose) message("sampled seed ", seed)

		cands_gap = which(outgoing_weight_d2 == 0)
	}


#everyhing at distance 2 is covered - so we prefer sampling objects with
#more free second neighbors
	while(sum(outgoing_weight_d1==0)>0
	& max(uncovered_neigh[outgoing_weight_d1==0]) >= min_clust_size/2) {
		cands = which(outgoing_weight_d1 == 0)
		#print(cands)
		#print(outgoing_weight_d2[cands])
		if(length(cands) == 1) {
			seed = cands;
		} else {
#		    if(length(cands) > 500) {
#			cands = sample(cands, 500, replace=F)
#		    }
#		    uncovered_neigh = apply(m_knn[,cands], 2,
#				 function(x) {
#				    sum(x>0 & outgoing_weight_d1==0) } )
#		    max_uncovered_neigh = max(uncovered_neigh)
#
#		    names(uncovered_neigh) = cands
		    seed = cands[sample(length(cands), 1, prob=uncovered_neigh[cands]**3)]
#		    message("sample d1 with ", sum(outgoing_weight_d1 == 0), " uncovered, #seeds ", length(seeds), " seed ", seed, " with ", uncovered_neigh[as.character(seed)])
		    if(verbose) {
		      message(" seed ", length(seeds), " uncovered ", sum(outgoing_weight_d1 == 0))
		    }
		}
		seeds = c(seeds, seed)
		outgoing_mat[seed,] = m_knn[,seed]
		new_covs = (outgoing_weight_d1 == 0) & (outgoing_mat[seed,] > 0)
		if(verbose) { message("new covs ", sum(new_covs)) }
		outgoing_weight_d1 = outgoing_weight_d1 + outgoing_mat[seed,]  #colSums(outgoing_mat)
		uncovered_neigh = uncovered_neigh - new_covs %*% m_knn_01
		if(verbose) { message("after update, max uncov ", max(uncovered_neigh)) }
		if (verbose) message("sampled seed ", seed)
#		print(outgoing_weight_d1)
#		print(outgoing_weight_d2)
	}

	outgoing_weight_d2 = colSums(outgoing_mat %*% m_knn)
	# message("done sampling seeds")

#	d2 = t(m_knn) %*% t(outgoing_mat)

	if (verbose) message("done seeding")
#compute the cluster of each node by looking for the maximum weight seed neighbor
	clusts = apply(outgoing_mat, 2, function(x) {
			 ifelse(max(x)>0, which.max(x), -1)})
	names(clusts) = colnames(outgoing_mat)

	bad_clusts = .find_small_clusts(clusts, outgoing_mat, min_clust_size)
	# message("before killing small clusters ", sum(clusts==-1), " elements are unassigned")
	# message(length(bad_clusts), " small clusters to kill")

	if (length(bad_clusts) > 0) {
	  clusts[clusts %in% bad_clusts] = -1
	  outgoing_mat[bad_clusts,] = 0
	  # message("after killing small clusters ", sum(clusts==-1), " elements are unassigned")
	}

	orphan_members = names(which(clusts == -1))
	if(length(orphan_members) != 0) {
		outgoing_mat2 = outgoing_mat %*% m_knn
		clusts2 = apply(outgoing_mat2, 2, which.max)
		names(clusts2) = colnames(outgoing_mat)
		clusts[orphan_members] = clusts2[orphan_members]
	}
	# message("after orphan reassignment by 2 chains ", sum(clusts==-1), " elements are unassigned")
	clusts = as.integer(as.factor(clusts))
	names(clusts) = colnames(outgoing_mat)

	if(consolidate == T) {
		clusts = .graph_clust_consolidate(clusts, m_knn, min_clust_size)
	}

	return(clusts)
}

#cancel small clusts and reassign members
.find_small_clusts = function (clusts, outgoing_mat, min_clust_size) {
  cl_sz = table(clusts)
  bad_clusts = as.numeric(names(which(cl_sz<min_clust_size)))
  bad_clusts = bad_clusts[bad_clusts>0]
  return(bad_clusts)
}

# Not tested yet!
.graph_clust_consolidate = function(clusts, m_knn, min_clust_size,
			min_diff = NA, max_iter = 40, start_cooling = 10)
{
	C = length(clusts)
	if(is.na(min_diff)) {
		min_diff = 10
	}
	diff = min_diff+1
	new_clusts = clusts
	ncls = max(clusts)
	iter = 0
	reg = 1
	cooling_rate = 1.05

	start_cooling = max(start_cooling, 5)

	diff_traj = rep(0,max_iter)

	while(diff > min_diff & iter < max_iter)	{
		clusts = new_clusts
		clusts1 = diag(ncls)[,clusts]    # 1 encoding of the clusters, rows= clusters, cols =nodes
		csizes = rowSums(clusts1); csizes[csizes==0] = 1 # add 1 to avoid division by 0
		clusts1 = clusts1/csizes

		#vector of forward clust assoc for each member (which clusts do I like?)
		forward_votes = t(clusts1 %*% m_knn) *1000
		#vector of reverse clust assoc for each member (how many in the clust like me?)
		backward_votes = tcrossprod(m_knn, clusts1) * 1000

		# #vector of forward clust assoc for each member (which clusts do I like?)
		# forward_votes_old = t(apply(m_knn, 2, function(x) tapply(x, clusts, sum)/csize))
		# #vector of reverse clust assoc for each member (how many in the clust like me?)
		# backward_votes_old = t(apply(m_knn, 1, function(x) tapply(x, clusts, sum)/csize))

		votes = backward_votes * forward_votes + 1e-10 * forward_votes  # + 1e-10 * forward_votes is for nodes that no one wants

		# add regularization to make nodes stay in their cluster
		if (iter > start_cooling) {
			if((diff_traj[iter-1]/diff_traj[iter-3]) > 0.75) {
				reg = reg * cooling_rate
				# message("cooling to ", reg)
			}
			if(reg > 1) {
				idx = nrow(votes)*(clusts-1) + 1:nrow(votes)
				votes[idx] = reg * votes[idx]
			}
		}

		new_clusts = apply(votes,1,which.max)

		diff = sum(new_clusts != clusts)
		diff_traj[iter] = diff
		message(" i ", iter, " d=",diff)
		iter=iter+1
	}
# TODO: add code to eliminate small clusters, reassign orphand, re-number clusters in case some are missing
#	bad_clusts = .find_small_clusts(clusts, outgoing_mat, min_clust_size)
#	if (length(bad_clusts) > 0) {
#	  new_clusts[clusts %in% bad_clusts] = -1
#	}
	message("done consolidating")

	return(new_clusts)
}

