#' scrdb pipeline
#'
#' pipeline for reading mars-seq, clustering.  Plotting is done by sc_pipe_plots
#'
#' @return returns invisibly a tgScMatClust2D plotting object.
#'
#' @export
sc_pipe = function(
	param_fn = NA,
	index_fn = "batch_index.txt",
	base_dir = ".",
	batch_meta_attr = "amp.batch",
	mars_batches = NULL,
	sample_n_batches = NA,
	outdir = ".",
	outdir_add_timestamp = F,
	clust_knn = 150,
	min_umi_n = 500,
	min_umis_init = min(200, min_umi_n),
	mark.min_var_mean = 0.2,
	mark.sz_cor_norm_max= -Inf,
	mark.niche_T = Inf,
	mark.min_tot = NA,
	glob_blacklist_terms = c(),
	mark_blacklist_terms = c(),
	outlier_gene_top1 = 20,
	outlier_gene_on_count = 200,
	remove_outliers_before_clustering = F,
	min_clust_size = 20,
	filt_amb_on_clusts = F,
	filt_outliers_on_clusts = F,
	amb_epsilon = 0.03,
	store_rda_fn = NA,
	tab_clust_fp_fn = "clust_fp.txt",
	sort_types_markers_fn=NULL,
	sort_ref_centers_pref_fn=NULL,
	cell_subset=NA,
	additional_markers=c()
)
{
	if(!is.na(param_fn)) {
		source(param_fn, local=T)
	}

	if(outdir_add_timestamp) {
		outdir = paste0(outdir,"/",format(Sys.time(), "%Y.%m.%d_%H.%M"))
	}
  
	if(!dir.exists(outdir)){
		dir.create(outdir)
	}

	# read batch index
	index = read.delim(index_fn, stringsAsFactors=F, header=T)

	# use selected batches
	if(!is.null(mars_batches)) {
		bat = intersect(mars_batches, index[, batch_meta_attr])
		index = index[index[,batch_meta_attr] %in% bat,]
	}

	# sample batches
	if(!is.na(sample_n_batches)) {
		sample_n_batches = min(sample_n_batches, nrow(index))
		index = index[sample(1:nrow(index), sample_n_batches, replace=F),]
	}

	# read all mars UMIs
	sc_mat = scm_read_scmat_mars(base_dir=base_dir,
				 mars_batches = index[, batch_meta_attr],
				 batch_meta = index,
				 min_umis_n = min_umis_init)


				 
				 
	sc_mat_orig = sc_mat

	if(length(glob_blacklist_terms) > 0) {
	# blacklist for markers
		glob_blist = c()
		allg = rownames(sc_mat@mat)
		for(bad in glob_blacklist_terms) {
			glob_blist = union(glob_blist, grep(bad, allg, v=T))
		}
		if(length(glob_blist) > 0) {
			sc_mat = scm_sub_mat(sc_mat,
			 genes = setdiff(rownames(sc_mat@mat), glob_blist))
		}
	}

	# remove cross well contamination
	sc_mat = scm_remove_ambient_by_epsilon(sc_mat, amb_epsilon, batch_meta_attr, min_umi_n = min_umi_n)
	# calc gstat
	
	if(!is.na(cell_subset)) {
		sc_mat@mat=sc_mat@mat[,cell_subset]
		sc_mat@cells=cell_subset
		sc_mat@ncells=length(cell_subset)
		sc_mat@cell_metadata=sc_mat@cell_metadata[cell_subset,]
	}
	
	
	
	
	
	gstat = umi_gene_stat(sc_mat)

	# remove_outliers (extract to scmat?)
	if (remove_outliers_before_clustering) {
		outlier_genes = rownames(gstat[ gstat$ds_top1 > outlier_gene_top1
						& gstat$is_on_count < outlier_gene_on_count,])
		if(length(outlier_genes) > 1) {
			outlier_cells = names(which(colSums(as.matrix(sc_mat@mat)[outlier_genes,])>outlier_gene_top1))
			message("removing ",length(outlier_cells), " outlier cells and ", length(outlier_genes), " outlier genes")
			if(length(outlier_cells) > 0) {
				sc_mat = scm_sub_mat(sc_mat,
					 cells = setdiff(colnames(sc_mat@mat), outlier_cells),
					 genes = setdiff(rownames(sc_mat@mat), outlier_genes))
			}
			gstat = gstat[setdiff(rownames(gstat),outlier_genes),]
		}
	}

	
	# markers
	marks = select_markers(sc_mat,gstat = gstat,
				 type="any",
				 mark.min_var_mean = mark.min_var_mean,
				 mark.sz_cor_norm_max=mark.sz_cor_norm_max,
				 mark.niche_T = mark.niche_T,
				 mark.min_tot = mark.min_tot)

	# blacklist for markers
	blist = c()
	allg = rownames(gstat)
	for(bad in mark_blacklist_terms) {
		blist = union(blist, grep(bad, allg, v=T))
	}

	#additional_markers
	if(length(additional_markers) > 0){
		to_add=intersect(rownames(sc_mat@mat),additional_markers)
		marks=union(marks,to_add)	
		message(paste0("I added ",length(to_add),"  custom markers"))
		message(paste0("Total markers: ",length(marks)))
	}
	
	marks = setdiff(marks, blist)
	if(length(marks) < 10) {
		stop("found ", length(marks), " markers only, consider changing config to allow more genes qualifying as informative")
	}
	plot_markers(mat = sc_mat@mat,marks =  marks, gstat = gstat, fname = "markers.png", outdir = outdir)

	# in-silico sort to types by reference type markers and cluster centers
  if (!is.null(sort_ref_centers_pref_fn)) {
    message(sprintf("in-silico sorting cells based on cell type markers: %s and pre-defined clusters %s", sort_types_markers_fn, sort_ref_centers_pref_fn))
    stopifnot(!is.null(sort_types_markers_fn))
    sc_mat = sc_marker_split(sc_mat, sort_types_markers_fn, sprintf("%s.txt", sort_ref_centers_pref_fn), sprintf("%s_assign.txt", sort_ref_centers_pref_fn), fig_pref=sprintf("%s/sort", outdir))  
  }	
	
	###########
	# cluster #
	###########
	sc_cl = scc_cluster_knn_graph(sc_mat, sc_mat@mat[marks,], k_knn=clust_knn, min_clust_size=min_clust_size)

	reclust = F
	if(filt_amb_on_clusts) {
		deamb_mat = .scc_clean_ambient_on_clusts(sc_cl, sc_mat_orig@mat[sc_mat@genes,], amb_epsilon, batch_meta_attr)
		sc_mat = tgScMat(deamb_mat, "umi", cell_metadata = sc_mat@cell_metadata[colnames(deamb_mat),])
		reclust = T
	}
	cell_outliers_nms = c()
	
	if(filt_outliers_on_clusts) {
		cell_outliers_nms = find_clustering_outliers(sc_cl)
		message("remove ", length(cell_outliers_nms), " outlier cells")
		sc_mat = scm_sub_mat(sc_mat, cells=setdiff(colnames(sc_mat@mat), cell_outliers_nms))
		reclust = T
	}
	sc_cl_1st = sc_cl
	if(reclust) {
		sc_cl = scc_cluster_knn_graph(sc_mat, sc_mat@mat[marks,], k_knn=clust_knn, min_clust_size=min_clust_size)
	}

	if(!is.na(tab_clust_fp_fn)) {
		f = apply(sc_cl@clust_fp,1,max)>1.5;
		write.table(round(sc_cl@clust_fp[f,],2),  paste0(outdir, "/",tab_clust_fp_fn), sep="\t", quote=F)
	}

	if(!is.na(store_rda_fn)) {
		message("saving Rda")
		save(sc_mat, sc_mat_orig, sc_cl, sc_cl_1st, cell_outliers_nms, gstat, file=paste0(outdir,"/",store_rda_fn))
	}
	invisible(sc_cl)
}

#' pipeline for reading mars-seq, clustering.  Plotting is done by sc_pipe_plots
#'
#' @return returns invisibly a tgScMatClust2D plotting object.
#'
#' @export
sc_pipe_plots = function(sc_cl,
	outdir = ".",
	param_fn = NA,
	mark_blacklist_terms = c(),
	mark_colors_fn = NA,
	fig2d_force_comp = F,
	fig_width_confu = 1200,
	fig_width_mat = 2000,
	fig_mat_per_clust_genes=10,
	fig_mat_gene_min_fold=2.5,
	fig_mat_gene_min_cov=0.5,
	fig_mat_width = 2000,
	fig_mat_height  = 3500,
	fig_mat_text_cex=0.8,
	fig_mat_smooth_n=10,
	fig_2d_height=1500,
	fig_2d_width = 1500,
	K_2dproj = 15,
	K_cells_2dproj = 15,
	force_max_deg_2dproj = 4,
	T_edge_2dproj=0.05,
	T_edge_asym=F,
	restrict_edges_by_fpcor=T,
	expand_K_connect_2dproj=10,
	fold_shades = colorRampPalette(rev(RColorBrewer::brewer.pal(11,"RdBu")))(1000), #(c("black", "blue", "lightblue", "white", "orange", "red", "yellow"))(1000),
	store_rda_fn = NA,
	focus_tfs_fn = NA,
	plot_genes_2d = F,
	meta_field_nm = NA
) {
	if(!is.na(param_fn)) {
		source(param_fn, local=T)
	}
	blist = c()
	allg = sc_cl@scmat@genes
	for(bad in mark_blacklist_terms) {
		blist = union(blist, grep(bad, allg, v=T))
	}
	##########
	#  plot  #
	##########
	sc_2d = tgScMatClustLayout2D(sc_cl)
	sc_2d = scp_compute_clust_knn_graph(sc_2d,
					force_max_deg = force_max_deg_2dproj,
					K = K_2dproj,
					T_edge = T_edge_2dproj,
					T_edge_asym = T_edge_asym,
					subselect_by_fpcor=restrict_edges_by_fpcor,
					k_expand_inout_factor=expand_K_connect_2dproj)

	if(!is.na(mark_colors_fn)) {
		mcol = read.table(mark_colors_fn, sep="\t", h=T, stringsAsFactors=F)
		marker_colors = mcol$color
		names(marker_colors) = mcol$TF
		sc_2d = scp_set_clust_cols(sc_2d, marker_colors = marker_colors)
	}

	sc_2d = scp_plot_confusion_mat(sc_2d,outdir=outdir, height=fig_width_confu, width=fig_width_confu)

	sc_2d = scp_plot_mat(sc_2d,png_fn=paste0(outdir,"/all_mat.png"),
				 per_clust_genes = fig_mat_per_clust_genes,
				 gene_min_fold = fig_mat_gene_min_fold,
				 gene_min_cov = fig_mat_gene_min_cov,
				 width = fig_mat_width, height = fig_mat_height,
				 text_cex = fig_mat_text_cex,
				 smooth_n = fig_mat_smooth_n,
				 blacklist=blist)
	sc_2d = scp_plot_mat(sc_2d, plot_cells = F,
				 png_fn=paste0(outdir,"/clust_mat.png"),
				 fp_shades = fold_shades,
				 per_clust_genes = fig_mat_per_clust_genes,
				 gene_min_fold = fig_mat_gene_min_fold,
				 gene_min_cov = fig_mat_gene_min_cov,
				 width = fig_mat_width, height = fig_mat_height,
				 text_cex = fig_mat_text_cex,
				 blacklist=blist)

	# plot metadata
	if(is.na(meta_field_nm)) {
		meta_field_nm = colnames(sc_2d@scl@scmat@cell_metadata)[1]
	}
	sc_2d = scp_plot_metadata_factor(sc_2d, clust_breakdown = T, heatmap = T, meta_field_nm = meta_field_nm,
									 width = fig_mat_width, height = fig_mat_width/2,fname = paste0(outdir,"/metadata.png"))

	# projections
	sc_2d = scp_plot_clust_2d(sc_2d,fname=paste0(outdir,"/all_2d.png"),
				height=fig_2d_height, width=fig_2d_width,
				K_for_cells = K_cells_2dproj)
	sc_2d = scp_plot_clust_2d(sc_2d,fname=paste0(outdir,"/graph_2d.png"),
				height=fig_2d_height, width=fig_2d_width,
				plot_edges = T,
				K_for_cells = K_cells_2dproj)

	# TF plots
	if(!is.na(focus_tfs_fn)) {
		tfs = read.table(focus_tfs_fn, h=T)
		tfs = rownames(tfs)
		sc_2d = scp_plot_mat(sc_2d,png_fn=paste0(outdir,"/tf_all_mat.png"),
					 genes_pool = tfs,
					 per_clust_genes = fig_mat_per_clust_genes,
					 gene_min_fold = fig_mat_gene_min_fold,
					 gene_min_cov = fig_mat_gene_min_cov,
					 width = fig_mat_width, height = fig_mat_height,
					 text_cex = fig_mat_text_cex,
					 smooth_n = fig_mat_smooth_n,
					 blacklist=blist)

		sc_2d = scp_plot_mat(sc_2d, plot_cells = F,
					 genes_pool = tfs,
					 png_fn=paste0(outdir,"/tf_clust_mat.png"),
					 fp_shades = fold_shades,
					 per_clust_genes = fig_mat_per_clust_genes,
					 gene_min_fold = fig_mat_gene_min_fold,
					 gene_min_cov = fig_mat_gene_min_cov,
					 width = fig_mat_width, height = fig_mat_height,
					 text_cex = fig_mat_text_cex,
					 blacklist=blist)

		cl_ord = sc_2d@cl_ord

		covgenes = names(rowSums(sc_cl@scmat@mat)>80)
		tfs = intersect(tfs, covgenes)
		tfs = intersect(tfs, rownames(sc_cl@clust_fp))
		mask_folds = sc_cl@clust_fp[tfs,]*(sc_cl@clust_gcov[tfs,]>0.5)
		spec_tfs = names(which(apply(mask_folds, 1, max)>2))

		tf_cor = cor(log2(t(sc_cl@clust_fp[spec_tfs,])))

		tf_hc = hclust(dist(tf_cor), "ward.D2")
		tf_max_at = apply(sc_cl@clust_fp[spec_tfs,cl_ord],1,which.max)
		tf_hc = as.hclust(reorder(as.dendrogram(tf_hc), tf_max_at, agglo.FUN=mean))
		png(paste0(outdir,"/tfs_hclust.png"), w=1500, h=1500)
		plot(as.phylo(tf_hc), type="fan")
		dev.off()
		png(paste0(outdir,"/tfs_cor.png"), w=2000, h=2000)
		diag(tf_cor) = NA
		tfs_shades = colorRampPalette(c("blue", "blue", "blue", "lightblue", "white", "red", "orange", "yellow", "black"))(1000);
		image(tf_cor[tf_hc$order, tf_hc$order], col=tfs_shades, zlim=c(-1,1), xaxt='n', yaxt='n')
		mtext(spec_tfs[tf_hc$order], at=seq(0,1,l=length(spec_tfs)), las=2, side=2, cex=0.8)
		mtext(spec_tfs[tf_hc$order], at=seq(0,1,l=length(spec_tfs)), las=2, side=4, cex=0.8)
		mtext(spec_tfs[tf_hc$order], at=seq(0,1,l=length(spec_tfs)), las=2, side=1, cex=0.8)
		dev.off()
	}

	if(plot_genes_2d) {
		message("plotting genes 2d")
		if (!exists("spec_tfs")) {
			spec_tfs = marks
		}
		dir.create(paste0(outdir,"/genes/"))
		for(nm in spec_tfs) {
			cat("plotting ",nm,"\n")
			scp_plot_gene_2d(sc_2d, gene_nm=nm,base_dir=paste0(outdir,"/genes/"), w=1200, h=1200,reg_factor=10)
		}
	}

	if(!is.na(store_rda_fn)) {
		message("saving Rda")
		save(sc_2d,  file=paste0(outdir,"/",store_rda_fn))
	}
	invisible(sc_2d)

}
