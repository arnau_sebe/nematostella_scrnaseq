library(zoo)
library(plyr)
library(dplyr)

load_global_and_local_motif_max_quant=function(global_motif_quant_table_file="./Motif_analysis_input_tables/CisBP_global_motif_max_val_quant_250bp.txt",
                                     feature_motif_quant_table_file="./Motif_analysis_input_tables/CisBP_max_promoters200_50.txt",
                                     gene_lists_table_file="./Motif_analysis_input_tables/Nvec_gene_lists_per_cluster",               
                                     dict_file="./Motif_analysis_input_tables/Nvec_CisBP.dictionary"){  
  Global_motif_max_quant=read.table(global_motif_quant_table_file,header=T,row.names=1,sep="\t")
  Global_motif_max_quant<<-Global_motif_max_quant
 
  Promoter_max_motifs=read.table(feature_motif_quant_table_file,header=T,sep="\t")
  Promoter_max_motifs<<-Promoter_max_motifs
  
  dict_motifs_annot=read.table(dict_file,header=T,sep="\t",fill=TRUE,quote="",row.names=1)
  dict_motifs_annot<<-dict_motifs_annot
  
  
  Gene_lists_table=read.table(gene_lists_table_file,header=T,sep="\t")
  Gene_lists_table<<-Gene_lists_table
}

##Compute ONCE motif-motif correlation based on promoter energies and save the table.
motif_cor_reduce_motifs=function(output_table_name="Motif_motif_cor_table.txt",plot=FALSE){
  Promoter_max_motifs_norm=quantile_normalization(Promoter_max_motifs[,6:ncol(Promoter_max_motifs)])
  cor_motifs=cor(Promoter_max_motifs_norm)
  
  write.table(cor_motifs,file=output_table_name,quote=FALSE,sep="\t",row.names=T,col.names=T)
  
  if(plot){
  hc_motifs=hclust(dist(cor_motifs),method="ward.D2")
  png("Reduced_motifs.png",height=15000,width=15000)
  par(mar=c(0,0,0,0))
  par(fig=c(0.097,0.903,0.85,0.95))
  plot(hc_motifs,xaxt='n',yaxt='n', xaxs="i",sub="",ylab="",cex=0.7,main="",xlab="",lwd = 3,hang=-1,labels=FALSE)
  par(fig=c(0.1,0.9,0.05,0.85),new=TRUE)
  image(cor_motifs[hc_motifs$order,hc_motifs$order],col=colorRampPalette(c("white","white","white","red","darkred"))(1000),xaxt='n',yaxt='n')
  mtext(dict_motifs_annot[rownames(cor_motifs),"gene_annot"], side=2,at=seq(0,1,length.out=nrow(cor_motifs)), las=2,cex=1)
  mtext(dict_motifs_annot[rownames(cor_motifs),"gene_annot"], side=4,at=seq(0,1,length.out=nrow(cor_motifs)), las=2,cex=1)
  dev.off()
  }
}


##Compute motif FDR per genes in each cell cluster
motif_enrichment_permutation=function(Quant_thresh=0.98,Motifs_to_test="ALL",output_table_bn,
                                      gene_list=NULL,energies=Promoter_max_motifs,
                                      repetitions=100){    
  
  if(Motifs_to_test=="ALL"){ #default
     Motifs_to_test=colnames(energies)[grep("_CisBP",colnames(energies))]
  }
  
  Niches=as.character(unique(Gene_lists_table$niche))
  Niches=intersect(Niches,names(which(table(Gene_lists_table$niche)>5)))
  Reduced_energies_matrix=energies[,c("chrom","start","end","strand","gene_id",Motifs_to_test)]
  
  Fdr_matrix=matrix(ncol=length(Niches),nrow=length(Motifs_to_test))
  colnames(Fdr_matrix)=Niches;rownames(Fdr_matrix)=Motifs_to_test
  
  permuted_energies=energies[,c("chrom","start","end","strand","gene_id",Motifs_to_test)]  
  for(Niche in Niches){
    Genes=as.character(Gene_lists_table[which(Gene_lists_table$niche==Niche),"gene_ID"])
    Genes=intersect(Genes,unique(energies$gene_id))
    
    pvs=motif_enrichment_p.val_matrix(gene_list=Genes,energies=Reduced_energies_matrix)
    s_pvs = sort(pvs)    
    Np = length(pvs)
    
    r_pvs=c()
    for(i in 1:repetitions) {
      permuted_energies$gene_id=as.character(permuted_energies$gene_id[sample(nrow(permuted_energies))])
      r_pvs = c(r_pvs, motif_enrichment_p.val_matrix(Quant_thresh=Quant_thresh,gene_list=Genes,
                                                     energies=permuted_energies))

    }
    rnks = rank(c(s_pvs, r_pvs))[1:Np]    
    fd = rnks - 1:Np    #the rank of each real p-value (minus its rank in the real p-values) represents how many "random" cases were better
    fdr = fd/(repetitions*Np) #the FDR is the fraction of fd from the real pvs   
    pvs[order(pvs)] = fdr  #we have to project it back to the original p-values    
    Fdr_matrix[names(pvs),Niche]=pvs
  }
  Fdr_matrix<<-Fdr_matrix
  write.table(Fdr_matrix,file=paste(output_table_bn,Quant_thresh,"FDR_table.txt",sep="_"),row.names=T,col.names=T,sep="\t",quote=FALSE)
}


  
motif_enrichment_clustering=function(output_fn="Promoter_motif_enrichment_heatmap.png",feature_type,  #feature type can be "promoters" or "enhancers"                         
                                     motif_merge_cor_thrs=0.8,Quant_thresh=0.98,
                                     motif_FDR_table_file="./Motif_analysis_input_tables/Precomputed_Cell_clusters_Promoter_motifs_FDR.txt",fdr_threshold=0.02,
                                     features_table=Promoter_max_motifs,
                                     cor_matrix_file="Motif_motif_cor_table.txt",
                                     write_tables=FALSE){
   
  cor_motifs=read.table(cor_matrix_file,header = T,row.names=1,sep="\t")

  
  Niches=as.character(unique(Gene_lists_table$niche))
  
  motif_FDR_table=read.csv(motif_FDR_table_file,row.names=1,header=T,sep="\t",check.names=FALSE)
  colnames(motif_FDR_table)=as.character(colnames(motif_FDR_table))
  motif_FDR_table=motif_FDR_table[,1:ncol(motif_FDR_table)-1]
  
  mins=sort(apply(motif_FDR_table,1,function(x) min(x))) #sort motifs by min FDR in any niche
    
  Motifs_to_use=names(which(mins<fdr_threshold))  ##sorted motifs (important for redundancy reduction) below the FDR.

  matrix_SN_motif=matrix(nrow=length(Motifs_to_use),ncol=length(Niches))
  colnames(matrix_SN_motif)=Niches;rownames(matrix_SN_motif)=Motifs_to_use  
  matrix_SN_motif_fg_counts=matrix_SN_motif;matrix_SN_motif_bckg_counts=matrix_SN_motif;matrix_SN_motif_bckg_frac=matrix_SN_motif;matrix_SN_motif_fg_frac=matrix_SN_motif
  
  for (SN in Niches){
        genes=intersect(as.character(Gene_lists_table[which(Gene_lists_table$niche==as.character(SN)),"gene_ID"]),unique(features_table$gene_id))
        bckg_genes=as.character(setdiff(unique(features_table$gene_id),genes))
        

        fg_counts=sapply(Motifs_to_use,function(x) sum(features_table[which(features_table$gene_id %in% genes),x]>Global_motif_max_quant[as.character(Quant_thresh),paste0(x,"_max")]))
        
        bckg_counts=sapply(Motifs_to_use,function(x) sum(features_table[which(features_table$gene_id %in% bckg_genes),x]>Global_motif_max_quant[as.character(Quant_thresh),paste0(x,"_max")]))
                
        fg_frac=fg_counts/length(genes)
        bckg_frac=bckg_counts/length(bckg_genes)           
        matrix_SN_motif[Motifs_to_use,SN]=log2((fg_frac)/(bckg_frac)) 
        matrix_SN_motif_fg_counts[Motifs_to_use,SN]=fg_counts
        matrix_SN_motif_bckg_counts[Motifs_to_use,SN]=bckg_counts
        matrix_SN_motif_fg_frac[Motifs_to_use,SN]=fg_frac
        matrix_SN_motif_bckg_frac[Motifs_to_use,SN]=bckg_frac
        #message(paste0("...done with cell cluster ",SN,"..."))
  }    
  matrix_SN_motif[is.infinite(matrix_SN_motif)]=0
  
  #######################################REDUCE MOTIF REDUNDANCY#######################################  
  #sort motifs by high FC value before reduction:
  Motifs_to_use_sorted=Motifs_to_use[order(apply(matrix_SN_motif[intersect(rownames(matrix_SN_motif),Motifs_to_use),],1,function(x) max(rollmean(x,4))),decreasing =TRUE)]
  
  red_cor_motif=cor_motifs[Motifs_to_use_sorted,Motifs_to_use_sorted];diag(red_cor_motif)=0  
  red_cor_motif[upper.tri(red_cor_motif)]=0
  motifs_low_cor=rownames(red_cor_motif[which(apply(red_cor_motif,1,max)<motif_merge_cor_thrs),])
  
  hc_motifs=hclust(as.dist(1-cor_motifs),method="ward.D2")
  cls<-cutree(hc_motifs, k=length(motifs_low_cor))  
  cluster_limits <- list()
  for(k in 1:length(motifs_low_cor)) {
       cluster_limits[[k]] <- names(cls[cls==k])
   }
  
  clust_limits=cluster_limits[unique(cls[hc_motifs$order])]
  #######################################

  matrix_SN_motif_2=matrix_SN_motif
  
  if(feature_type=="enhancers"){
      message("...analysing motif enrichment in enhancers...")
      matrix_SN_motif_2[which(matrix_SN_motif_fg_counts<5 & matrix_SN_motif>0)]=0   
      matrix_SN_motif_2[which(matrix_SN_motif_bckg_counts<100 & matrix_SN_motif>0)]=0  
  }else if(feature_type=="promoters"){
      message("...analysing motif enrichment in promoters...")
      matrix_SN_motif_2[which(matrix_SN_motif_fg_frac<quantile(matrix_SN_motif_fg_frac,0.1))]=0
      matrix_SN_motif_2[which(matrix_SN_motif_bckg_frac<quantile(matrix_SN_motif_bckg_frac,0.1))]=0
  }else{
      message("...UNKNOWN FEATURE TYPE...")
  }

  matrix_SN_motif_2=matrix_SN_motif_2[names(which(rowSums(matrix_SN_motif_fg_counts)!=0)),] ##Kill rows with all 0s
  
  matrix_SN_motif_2=matrix_SN_motif_2[intersect(rownames(matrix_SN_motif_2),motifs_low_cor),]
  matrix_SN_motif_3=matrix_SN_motif_2[names(which(apply(matrix_SN_motif_2,1,max)>=1.5)),]   ###ask for strong FC
  
 
  Motif_order = rownames(matrix_SN_motif_3)[order(apply(matrix_SN_motif_3[intersect(rownames(matrix_SN_motif_3),
                                                      motifs_low_cor),], 1, function(x) which.max(rollmean(x,3))))]
  
  
  reduced_motifs_annot=matrix(ncol=3,nrow=length(Motif_order))
  rownames(reduced_motifs_annot)=Motif_order;colnames(reduced_motifs_annot)=c("gene_ID","gene_annot","merged_PWMs")
  for (motif in Motif_order){        
    pwms=names(unlist(clust_limits[grep(motif,lapply(clust_limits,names))]))
    pwms=unique(c(motif,pwms))
    merged_ids=paste(unique(dict_motifs_annot[pwms,"gene_ID"]),collapse="%%")
    merged_annots=paste(unique(dict_motifs_annot[pwms,"gene_annot"]),collapse="%%")
    merged_pwm=paste(pwms,collapse="%%")
    reduced_motifs_annot[motif,]=c(merged_ids,merged_annots,merged_pwm)
  }
  
  if(write_tables==TRUE){
     write.table(reduced_motifs_annot,file="Motifs__in_heatmap_ANNOTATION_TABLE.txt",quote=FALSE,col.names=TRUE,row.names=TRUE,sep="\t")
     write.table(matrix_SN_motif_3,"Motifs_plotted_matrix.txt",col.names=TRUE,row.names=TRUE,quote=FALSE,sep="\t")
  }
  png(output_fn,h=3000,w=3000)
  par(mar=c(0,0,0,0))
  par(fig=c(0.1,0.8,0.1,0.95))
  color=colorRampPalette(c("#F7F7F7","#F7F7F7","#F7F7F7","#A1D76A","darkorange","darkred"))(1000)
  image(t(pmin(pmax(matrix_SN_motif_3[Motif_order,],0),2.3)),col=color,xaxt='n',yaxt='n')
  mtext(Motif_order, side=2,at=seq(0,1,length.out=length(Motif_order)), las=2,cex=1,line=2)
  mtext(reduced_motifs_annot[Motif_order,"gene_annot"], side=4,at=seq(0,1,length.out=length(Motif_order)), las=2,cex=1,line=2)
  
  par(fig=c(0.1,0.3,0.03,0.07),new=TRUE)  
  image(x=seq(0,max(matrix_SN_motif_3),(2*max(matrix_SN_motif_3))/(length(color)-1)), 
        y=c(0,1), col=color, yaxt="n", z=matrix(nrow=length(color)/2,ncol=1,data=c(1:length(color))),
        ylab="",xlab="",cex.axis=3)
  mtext("log2 FC",side=1,line=5,cex=3)
  
  par(fig=c(0.1,0.8,0.08,0.1),new=TRUE)
  image(as.matrix(1:length(scr_sn_table[,"color"])),col=as.character(scr_sn_table[,"color"]), axes = F,xaxt='n',yaxt='n')  
  par(fig=c(0.1,0.8,0.95,0.97),new=TRUE)
  image(as.matrix(1:length(scr_sn_table[,"color"])),col=as.character(scr_sn_table[,"color"]), axes = F,xaxt='n',yaxt='n')
  

  dev.off()
 
 }



motif_enrichment=function(workdir=".",                           
                  Quant_thresh=0.98,
                 Superniche,p.val_thresh=0.001,
                 gene_list=NULL,print_table=FALSE,features=Promoter_max_motifs){
  setwd(workdir)
  if(is.null(gene_list)){
   Genes=as.character(Gene_lists_table[which(Gene_lists_table$niche==Superniche),"gene_ID"])  
  }else{
    Genes=gene_list
  }
  #Fg_genes_enrichment<<-Genes
  Genes=intersect(Genes,unique(features$gene_id))
  bckg_genes=as.vector(setdiff(unique(features$gene_id),Genes))

  
  fg_counts=sapply(colnames(features[which(features$gene_id %in% Genes),6:ncol(features)]),function(x) sum(features[which(features$gene_id %in% Genes),x]
                                                                     >Global_motif_max_quant[as.character(Quant_thresh),paste0(x,"_max")]))

  bckg_counts=sapply(colnames(features[which(features$gene_id %in%bckg_genes),6:ncol(features)]),function(x) sum(features[which(features$gene_id %in%bckg_genes),x]
                                                                             >Global_motif_max_quant[as.character(Quant_thresh),paste0(x,"_max")]))

  matrix=cbind.data.frame(fg_counts,bckg_counts,round(fg_counts*100/length(Genes),1),round(100*bckg_counts/length(bckg_genes),1))
  colnames(matrix)=c("fg_counts","bckg_counts","fg_perc","bckg_perc")

  ff=as.numeric(sapply(rownames(matrix),function(x) phyper(matrix[x,"fg_counts"]-1,matrix[x,"bckg_counts"],
                                              length(bckg_genes),length(Genes),lower.tail = FALSE)))
 

  matrix2=cbind.data.frame(matrix,log2(matrix$fg_perc/matrix$bckg_perc),ff,as.character(dict_motifs_annot[rownames(matrix),"gene_annot"]),as.character(dict_motifs_annot[rownames(matrix),"source"]))
  colnames(matrix2)=c("fg_counts","bckg_counts","fg_perc","bckg_perc","log2FC","p.value","TF name","source")

  matrix3=as.data.frame(matrix2[order(as.numeric(matrix2[,"p.value"])),])
  Gene_set_motif_enrichment<<-matrix3
  Gene_set_motif_enrichment_uniq<<-Gene_set_motif_enrichment[!duplicated(Gene_set_motif_enrichment[,"TF name"]),]

  Motifs=rownames(Gene_set_motif_enrichment_uniq[which(Gene_set_motif_enrichment_uniq[,"p.value"]<as.numeric(p.val_thresh)),])
  Motifs<<-Motifs

    if(print_table){
     library(xlsx)
     write.table(Gene_set_motif_enrichment_uniq,file=paste0("ME_table_",Superniche),col.names=TRUE,row.names=TRUE,quote=FALSE,sep="\t")
     write.xlsx(Gene_set_motif_enrichment_uniq,file=paste0("ME_table_",Superniche,".xlsx"),col.names=TRUE,row.names=TRUE)
    }
  
  return(ff)
}

motif_enrichment_p.val_matrix=function(Quant_thresh=0.98,                          
                          gene_list,energies){
  
  Genes=gene_list
  bckg_genes=as.vector(setdiff(unique(energies$gene_id),Genes))
  
  fg_counts=sapply(colnames(energies[which(energies$gene_id %in% Genes),6:ncol(energies)]),function(x) sum(energies[which(energies$gene_id %in% Genes),x]
                                                                                                           >Global_motif_max_quant[as.character(Quant_thresh),paste0(x,"_max")]))
  
  bckg_counts=sapply(colnames(energies[which(energies$gene_id %in%bckg_genes),6:ncol(energies)]),function(x) sum(energies[which(energies$gene_id %in%bckg_genes),x]
                                                                                                                 >Global_motif_max_quant[as.character(Quant_thresh),paste0(x,"_max")]))
   
  ff= phyper(fg_counts-1,bckg_counts,length(bckg_genes),length(Genes),lower.tail = FALSE)
  
  #return(names(which(ff<p.val_thresh)))
  return(ff)
  
}



#####PLOT MOTIF CORRELATION MATRIX
plot_motif_motif_correlation=function(correlation_matrix_file,cor_threshold=0.75,FDR_table=NULL){
  
  tfs=read.table("~/storage/scRNA_clustering/Nvec_new_clust/000TF_ANNOTATION_NEW",header=F,sep="\t",fill=TRUE,quote="",row.names=1)
  similarity_motifs=read.table(correlation_matrix_file,header=T,row.names=1,sep="\t")
  
  ####Kill rows and cols all 0s  
  similarity_motifs=similarity_motifs[which(rowSums(similarity_motifs)!=0),which(rowSums(similarity_motifs)!=0)]
  
  dict_motifs_annot=read.table("./Motif_analysis_input_tables/Nvec_CisBP.dictionary",header=T,sep="\t",fill=TRUE,quote="",row.names=1)
  
  hc_motifs=hclust(as.dist(1-similarity_motifs),method="ward.D2")
  
  if(is.null(FDR_table)){
        cls<-cutree(hc_motifs, k=ncol(similarity_motifs)/100)  
         cluster_limits <- list()
         for(k in 1:(ncol(similarity_motifs)/100)) {
            cluster_limits[[k]] <- names(cls[cls==k])
         }
         cluster.sizes = unlist(lapply(cluster_limits[unique(cls[hc_motifs$order])], length)) 
  }else  if(!is.null(FDR_table)){
  	######################Motif reduction similart to the one applied for promoters
  	motif_FDR_table=read.csv(FDR_table,row.names=1,header=T,sep="\t",check.names=FALSE)
  	colnames(motif_FDR_table)=as.character(colnames(motif_FDR_table))
  	motif_FDR_table=motif_FDR_table[,1:ncol(motif_FDR_table)-1]  
  	mins=sort(apply(motif_FDR_table,1,function(x) min(x))) #sort motifs by min FDR in any niche  
  	Motifs_to_use_sorted=intersect(names(mins),rownames(similarity_motifs))
  	red_sim_motif=similarity_motifs[hc_motifs$order,hc_motifs$order];diag(red_sim_motif)=0
  	red_sim_motif[upper.tri(red_sim_motif)]=0
  	motifs_low_cor=rownames(red_sim_motif[which(apply(red_sim_motif,1,max)<cor_threshold),])
  	##########################################
  
  	#cls=cutree(hc_motifs, k=length(motifs_low_cor))
        cls<-cutree(hc_motifs, k=length(motifs_low_cor))  
        cluster_limits <- list()
        for(k in 1:length(motifs_low_cor)) {
            cluster_limits[[k]] <- names(cls[cls==k])
         }
        cluster.sizes = unlist(lapply(cluster_limits[unique(cls[hc_motifs$order])], length)) 
  	best_motif_in_each_clust=names(unlist(lapply(cluster_limits[unique(cls[hc_motifs$order])],function(x) which.min(apply(motif_FDR_table[x,],1,min)))))
  }

  color=colorRampPalette(c("white","white","yellow","orange","red","darkred"))(1000)
  
  TF_ids=as.vector(dict_motifs_annot[rownames(similarity_motifs[hc_motifs$order,hc_motifs$order]),"gene_ID"])
  TF_ids2=gsub("_.*","",TF_ids)

  png("Motif_motif_correlation_matrix.png",height=5000,width=5000)
  par(mar=c(0,0,0,0))
  par(fig=c(0.1,0.9,0.1,0.85))
  image(as.matrix(similarity_motifs[hc_motifs$order,hc_motifs$order]),col=color,xaxt='n',yaxt='n')

  current.line = 0
  for (i in 1:length(cluster.sizes)){
    current.line = current.line + cluster.sizes[i]
    abline(v = current.line/(sum(cluster.sizes)-1) - 1/(2*sum(cluster.sizes)), col = "darkgrey", lwd = 2, lty = "dotted")
    abline(h = current.line/(sum(cluster.sizes)-1) - 1/(2*sum(cluster.sizes)), col = "darkgrey", lwd = 2, lty = "dotted")
  }  
  
  if(!is.null(FDR_table)){
  	par(fig=c(0.06,0.09,0.1,0.85),new=TRUE)
  	image(t(as.matrix(1:nrow(similarity_motifs))),col=ifelse(test = rownames(similarity_motifs[hc_motifs$order,hc_motifs$order]) %in% best_motif_in_each_clust,yes = "gray8",no="grey"), axes = F,xaxt='n',yaxt='n')
  }
  par(fig=c(0.93,0.95,0.1,0.85),new=TRUE)
  image(t(as.matrix(1:nrow(similarity_motifs))),col=as.character(tfs[TF_ids2,3]), axes = F,xaxt='n',yaxt='n')
  
  par(fig=c(0.85,0.9,0.07,0.09),new=TRUE)
  min=min(similarity_motifs);max=max(similarity_motifs)
  image(x=seq(min,max,(max - min)/(length(color)-1)), y=c(0,1), 
        col=color, yaxt="n", z=matrix(nrow=length(color),ncol=1,data=c(1:length(color))),ylab="",xlab="",cex.axis=1)
  
  dev.off()
  
}

plot_fg_bck_distributions=function(output,motif_list,superniche,quant_threshold_used=0.98){
  gdb.init("/net/mraid14/export/tgdata/db/tgdb/nvec/trackdb/")
  png(paste0(output,"_",superniche,"_dist.png"),h=9000,w=3000)
  par(mfrow=c(round(length(motif_list)/2),2))  
  
  Genes=as.character(Gene_lists_table[which(Gene_lists_table$niche==superniche),"gene_ID"])  
  bckg_genes=as.vector(setdiff(unique(Promoter_max_motifs$gene_id),Genes))
  for (motif in motif_list){
    par(mar=c(10,5,5,5))
    plot(density(Promoter_max_motifs[which(Promoter_max_motifs$gene_id %in% bckg_genes),motif]),main=paste(motif,dict_motifs_annot[motif,"gene_annot"],sep="_"),
         cex.axis=4,cex.main=4,lwd=10,xlab="")
    lines(density(Promoter_max_motifs[which(Promoter_max_motifs$gene_id %in% Genes),motif]),col="chartreuse",lwd=10)
    abline(v=Global_motif_max_quant[as.character(quant_threshold_used),paste0(motif,"_max")],lwd=9)
  }
  dev.off()
}  

plot_TSS_distribution_new=function(output,motif_list,superniches,quant_threshold_used=0.98){
  gdb.init("/net/mraid14/export/tgdata/db/tgdb/nvec/trackdb/")
  
  #par(mfrow=c(round(length(motif_list)/3),3))
  
  features=gintervals.load(intervals="promoters_200_50")  
  
  Genes=as.character(unique(Gene_lists_table[which(Gene_lists_table$niche %in% superniches),"gene_ID"]))
  Genes=intersect(Genes,unique(Promoter_max_motifs$gene_id))  
  
  Other_genes=as.vector(setdiff(unique(Promoter_max_motifs$gene_id),Genes))
  
  for (motif in motif_list){      
   
    q_thr=Global_motif_max_quant[as.character(quant_threshold_used),paste0(as.character(motif),"_max")]  
    
    #top_genes_1=as.character(Promoter_max_motifs[order(Promoter_max_motifs[which(Promoter_max_motifs$gene_id %in% Genes),motif],decreasing=TRUE)[1:500],"gene_id"])
    motif_genes=as.character(Promoter_max_motifs[which(Promoter_max_motifs[,motif]>q_thr),"gene_id"])
    fg_genes=intersect(motif_genes,Genes)    

    bckg_genes=intersect(motif_genes,Other_genes)
    
    gvtrack.create(vtrack = "motif_max",src=paste0("CisBP.",motif),func = "max")
    
    if(length(motif_genes>0)){      
      for (set in c("fg","bg")){
        
      if(set=="fg"){motif_genes=fg_genes}else{motif_genes=bckg_genes}  
      intervals_fg=features[which(features$gene_id %in% motif_genes),]
      #intervals=intervals[which(intervals$strand=="-1"),]
      intervals_fg=intervals_fg[which((intervals_fg$end-intervals_fg$start)==250),]
      intervals_fg$start=intervals_fg$start-400;intervals_fg$end=intervals_fg$end+350
      
      features_motifs_plus=gextract("as.numeric(sum(motif_max>q_thr))",invervals=intervals_fg[which(intervals_fg$strand=="1"),],iterator=100,colnames = "energy")      
      features_motifs_minus=gextract("as.numeric(sum(motif_max>q_thr))",invervals=intervals_fg[which(intervals_fg$strand=="-1"),],iterator=100,colnames = "energy")      
      features_motifs_minus$intervalID=rev(features_motifs_minus$intervalID+max(features_motifs_plus$intervalID))
      
      features_motifs=rbind.data.frame(features_motifs_plus,features_motifs_minus[rev(rownames(features_motifs_minus)),])
      
      cul=features_motifs %>% group_by(intervalID) %>% mutate(iter_id = rank(start)) %>% dcast(intervalID~iter_id,value.var="energy") %>% select(-intervalID)# %>% ungroup %>% select(-(chrom:end)) %>% spread(intervalID, iter_id, energy)
      cul[is.na(cul)]=0
      cul=cul[,-1]
      
        if(set=="fg"){fg_counts=cul}else{bckg_counts=cul}
      }
      #y=table(factor(x,levels=1:1050))
      fg_frac=colSums(fg_counts)/length(Genes)
      bg_frac=colSums(bckg_counts)/length(Other_genes)
      FC=log2((fg_frac+1)/(bg_frac+1))
      #FC[which(FC<0)]=0
      #FC[which(fg_frac<0.001)]=0
      png(paste0(output,"_",motif,"_TSSplot.png"),h=500,w=500)
      plot(y=fg_frac,x=(1:10)+0.5,type='l',lwd=6,cex.main=1,main=paste(motif,dict_motifs_annot[motif,"gene_annot"],sep="_"),
           xaxt='n',xlab="bp from TSS",ylab="Frequency",col="darkgreen")
      lines(y=bg_frac,x=(1:10)+0.5,lwd=6)
      abline(v=7,lwd=6,col="darkgrey")
      axis(side=1,at = 1:10,labels = seq(-600,300,by=100),las=2)  
      dev.off() 
    }
   
  }
 
  
}


library('Biostrings')
library('seqLogo')
library('msa')
library('motiflogo')
extract_motifs_pwm_seqlogo=function(motif_name,Superniche,intervals="promoters_200_50",
                                    pwms_file="/net/mraid14/export/tgdata/db/tgdb/nvec/trackdb//pssms/Nvec_CisBP",Quant_thresh=0.98){
  gdb.init("/net/mraid14/export/tgdata/db/tgdb/nvec/trackdb/")
  
  Fg_genes_enrichment=as.character(Gene_lists_table[which(Gene_lists_table$niche %in% Superniche),"gene_ID"])  
  
  pwms_data=read.table(paste0(pwms_file,".data"),header=F,sep="\t")
  pwms_keys=read.table(paste0(pwms_file,".key"),header=F,sep="\t")
  
  motif_pwm=pwms_data[which(pwms_data[,1]==pwms_keys[which(pwms_keys[,2]==gsub("_CisBP","",motif_name)),1]),3:6]
  motif_pwm=t(motif_pwm);rownames(motif_pwm)=c("A","C","G","T")
  
  Promoter_max_motifs_sub=Promoter_max_motifs[which(Promoter_max_motifs$gene_id %in% Fg_genes_enrichment),,drop=FALSE]
  Genes_w_motif=as.character(Promoter_max_motifs_sub[which(as.numeric(Promoter_max_motifs_sub[,motif_name])>
                                                     as.numeric(Global_motif_max_quant[as.character(Quant_thresh),paste0(motif_name,"_max")])),"gene_id"])
  
  features=gintervals.load(intervals)
  features_motifs=gextract(paste0("CisBP.",motif_name),invervals=features[which(features$gene_id %in% Genes_w_motif),],
                           iterator=1,colnames = "energy")
  
  motif_max_position=c();motif_max_value=c()
  for (gene in unique(features_motifs$intervalID)){
    motif_max_position[Genes_w_motif[gene]]=which.max(features_motifs[which(features_motifs$intervalID==gene),"energy"]) 
    motif_max_value[Genes_w_motif[gene]]=max(features_motifs[which(features_motifs$intervalID==gene),"energy"]) 
  }
  
  intervals_to_extract=features[which(features$gene_id %in% Genes_w_motif),]  
  intervals_to_extract$start=intervals_to_extract$start+motif_max_position-1;intervals_to_extract$end=intervals_to_extract$start+ncol(motif_pwm)
     
  seqs=toupper(gseq.extract(intervals = intervals_to_extract))
  seqs=gsub("N","A",seqs)
  ###Check out max PWM and RC sequence if necessary
  for (i in 1:length(seqs)){
      rc_seq=as.character(reverseComplement(DNAString(seqs[i])))
      score_seq=PWMscoreStartingAt(motif_pwm,seqs[i],starting.at = 1)
      score_rc_seq=PWMscoreStartingAt(motif_pwm,rc_seq,starting.at = 1)
      #print(paste0("######",score_seq,"  ",score_rc_seq))
      if(score_rc_seq > score_seq){ seqs[i]=rc_seq }
  }
   
  pwm = consensusMatrix(toupper(seqs),as.prob=TRUE)
  
  seqLogo(pwm,ic.scale = TRUE)
  seqLogo(motif_pwm,ic.scale = TRUE)
  
  x=motiflogo(pwm,tit=paste0(Gene_set_motif_enrichment[motif_name,"TF name"]," in ",Superniche))
  y=motiflogo(motif_pwm, tit = motif_name)
  #plot(density(motif_max_position),main=paste0(Gene_set_motif_enrichment[motif_name,"TF name"]," in ",Superniche),xlab = "Position")
 
  png(paste(motif_name,Superniche,".png",sep="."),height = 1000,width = 1000)  
  intervals_to_extract$start=intervals_to_extract$start-200;intervals_to_extract$end=intervals_to_extract$end+200
  seqs=toupper(gseq.extract(intervals = intervals_to_extract))
  seqs=gsub("N","A",seqs)
  ###Check out max PWM and RC sequence if necessary
  for (i in 1:length(seqs)){
    rc_seq=as.character(reverseComplement(DNAString(seqs[i])))
    score_seq=PWMscoreStartingAt(motif_pwm,seqs[i],starting.at = 1)
    score_rc_seq=PWMscoreStartingAt(motif_pwm,rc_seq,starting.at = 1)    
    if(score_rc_seq > score_seq){ seqs[i]=rc_seq }
  }  
  pwm_surroundings = consensusMatrix(toupper(seqs),as.prob=TRUE)
  z=motiflogo(pwm_surroundings,tit="Surroundings",scaleSize = 2)
  multiplot(x,y,z)
  dev.off()
}

compare_enh_prom_motifs_cor=function(){
  
  
  promoters=as.matrix(read.table("./Promoters_200_50_niche_level/Motifs_FC_niches.txt",header=T,row.names=1))
  promoters=promoters[intersect(Motifs_to_use_prom,rownames(promoters)),]
  
  enhancers=as.matrix(read.table("./Enhancer_analysis/Motifs_FC_niches_ENHANCERS.txt",header=T,row.names=1))
  enhancers=enhancers[intersect(Motifs_to_use_enh,rownames(enhancers)),]
  
  common_motifs=intersect(rownames(promoters),rownames(enhancers))
  
   prom_enh_cor=cor(t(promoters[common_motifs,]),t(enhancers[common_motifs,]))
  
  
  set.seed(43)
  clust_prom_enh=hclust(as.dist(1-prom_enh_cor),method="ward.D2")
  plot(clust_prom_enh)
  
  plot(t(promoters[common_motifs,]),t(enhancers[common_motifs,]))
  
  plot(density(diag(prom_enh_cor[common_motifs,common_motifs])),lwd=4)
  
  color=colorRampPalette(c("white","black"))(1000)
  image(pmax(t(prom_enh_cor[]),0),zlim=c(0,1),col=color,xaxt='n',yaxt='n')
  par(fig=c(0.85,0.9,0.07,0.09),new=TRUE)
  image(x=seq(0,1,(1 - 0)/(length(color)-1)), y=c(0,1), 
        col=color, yaxt="n", z=matrix(nrow=length(color),ncol=1,data=c(1:length(color))),ylab="",xlab="",cex.axis=1)
  
  
  image(pmax(t(prom_enh_cor[clust_prom_enh$order,clust_prom_enh$order]),0),col=color,zlim=c(0,1),xaxt='n',yaxt='n')
  
  
  v=c()
  for (i in 1:100){
    tmp_cor=diag(cor(t(promoters[common_motifs,sample(ncol(promoters))]),t(enhancers[common_motifs,sample(ncol(enhancers))])))
    v=c(v,tmp_cor)   
  }
  
  plot(density(v),lwd=4,col="darkred")
  lines(density(diag(prom_enh_cor)),lwd=4,col="darkgrey")
  
}


.calc_max_val_quantile <- function(track, size, groot, quantiles = c(0.9, 0.95,0.98, 0.99,0.995, 0.999)) {
  x <- sample(1:30, 1)
  system(paste("sleep", x))
  gsetroot(groot)
  options(gparam.type = "str")
  options(gmultitasking = FALSE)
  vtrack <- paste(track, "max", sep = "_")
  gvtrack.create(vtrack, track, "max")
  q <- gquantiles(vtrack, percentiles = quantiles, intervals = ALLGENOME[[1]], 
                  iterator = size)
  return(q)
}

.distrib_calc_motif_max_val_quantile <- function(size, groot, quantiles = c(0.85,0.9,0.95,0.98, 0.99, 0.995, 0.999), job.num, motifs_regex) {
  gsetroot(groot)
  motif_tracks <- gtrack.ls(motifs_regex)
  quantiles.str <- paste("c(", paste(quantiles, collapse = ","), ")")
  commands <- paste(".calc_max_val_quantile(\"", motif_tracks, "\",", size, ",", 
                    "\"", groot, "\",", quantiles.str, ")", sep = "")
  commands <- paste(commands, collapse = ",")
  res <- eval(parse(text = paste("gcluster.run(", commands, ",opt.flags='-l io_saturation=1 -q all.q' ,max.jobs=", job.num, 
                                 ")")))
  res <- ldply(res, function(x) x = x$retv) %>% t
  colnames(res) <- paste0(gsub(paste0(motifs_regex, '\\.'),  "", motif_tracks), "_max")
  rownames(res) <- quantiles
  return(res)
}

quantile_normalization <- function(df){
  df_rank <- apply(df,2,rank,ties.method="min")
  df_sorted <- data.frame(apply(df, 2, function(x) { sort(x, na.last=TRUE)}))
  df_mean <- apply(df_sorted, 1, mean, na.rm=TRUE)
  
  index_to_mean <- function(my_index, my_mean){
    return(my_mean[my_index])
  }
  
  df_final <- apply(df_rank, 2, index_to_mean, my_mean=df_mean)
  rownames(df_final) <- rownames(df)
  return(df_final)
}



motif_enrichment_permutation_shuffled_gene_lists=function(Quant_thresh=0.98,Motifs_to_test=rownames(motif_FDR_table),
                                      gene_list=NULL,energies=Promoter_max_motifs,
                                      repetitions=100){
  
  Superniches=as.character(unique(Gene_lists_table$niche))
  Superniches=intersect(Superniches,names(which(table(Gene_lists_table$niche)>5)))
  
  
  Gene_lists_table_shuffled=Gene_lists_table
  #Gene_lists_table_shuffled$gene_ID=as.character(sample(Gene_lists_table$gene_ID,size=length(Gene_lists_table_shuffled$gene_ID),replace=FALSE))
  
  Gene_lists_table_shuffled$gene_ID=as.character(sample(unique(energies$gene_id),size=length(Gene_lists_table$gene_ID),replace=FALSE))
  
  
  Reduced_energies_matrix=energies[,c("chrom","start","end","strand","gene_id",Motifs_to_test)]
  
  Fdr_matrix_shuff_genes=matrix(ncol=length(Superniches),nrow=length(Motifs_to_test))
  colnames(Fdr_matrix_shuff_genes)=Superniches;rownames(Fdr_matrix_shuff_genes)=Motifs_to_test
  
  permuted_energies=energies[,c("chrom","start","end","strand","gene_id",Motifs_to_test)]  
  
  for(Superniche in Superniches){
    Genes=as.character(Gene_lists_table_shuffled[which(Gene_lists_table_shuffled$niche==Superniche),"gene_ID"])
    Genes=intersect(Genes,unique(energies$gene_id))
    
    pvs=motif_enrichment_p.val_matrix(gene_list=Genes,energies=Reduced_energies_matrix)
    s_pvs = sort(pvs)    
    Np = length(pvs)
    
    r_pvs=c()
    for(i in 1:repetitions) {
      permuted_energies$gene_id=as.character(permuted_energies$gene_id[sample(nrow(permuted_energies))])
      r_pvs = c(r_pvs, motif_enrichment_p.val_matrix(Quant_thresh=Quant_thresh,gene_list=Genes,
                                                     energies=permuted_energies))
    }
    rnks = rank(c(s_pvs, r_pvs))[1:Np]    
    fd = rnks - 1:Np    #the rank of each real p-value (minus its rank in the real p-values) represents how many "random" cases were better
    fdr = fd/(repetitions*Np) #the FDR is the fraction of fd from the real pvs   
    pvs[order(pvs)] = fdr  #we have to project it back to the original p-values    
    Fdr_matrix_shuff_genes[names(pvs),Superniche]=pvs
    print(Superniche)
  }
  Fdr_matrix_shuff_genes<<-Fdr_matrix_shuff_genes
}

